package homework.part3_1.task6;
/*
Необходимо реализовать класс AmazingString, который хранит внутри себя
строку как массив char и предоставляет следующий функционал:
Конструкторы:
● Создание AmazingString, принимая на вход массив char
● Создание AmazingString, принимая на вход String
Публичные методы:
● Вернуть i-ый символ строки.
● Вернуть длину строки.
● Вывести строку на экран
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается массив char). Вернуть true, если найдена и false иначе
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается String). Вернуть true, если найдена и false иначе.
● Удалить из строки AmazingString ведущие пробельные символы, если они есть.
● Развернуть строку (первый символ должен стать последним, а последний первым и т.д.)
 */

public class AmazingString {
    private char[] string;

    /**
     * Конструктор с массивом char
     */
    public AmazingString(char[] string) {
        this.string = string;
    }

    /**
     * Конструктор со строкой String
     */
    public AmazingString(String string) {
        this.string = new char[string.length()];
        for (int i = 0; i < string.length(); i++) {
            this.string[i] = string.charAt(i);
        }
    }

    /**
     * Вывести строку на экран
     */
    public void printString() {
        for (int i = 0; i < this.string.length; i++) {
            System.out.print(this.string[i]);
        }
        System.out.println();
    }

    /**
     * Возвращает длину строки
     */
    public int lengthString() {
        return this.string.length;
    }

    /**
     * Возвращает i-ый символ строки
     */
    public char symbolAt(int i) {
        if (i < lengthString() && i >= 0)
            return this.string[i];
        else
            return ' ';
    }

    /**
     * Проверяет наличие подстроки в виде массива char
     */
    public boolean containsString(char[] subString) {
        boolean flag;
        for (int i = 0; i < (lengthString() - subString.length + 1); i++) {
            flag = true;
            if (subString[0] == string[i]) {
                for (int j = 0; j < subString.length; j++) {
                    if (subString[j] != string[j + i])
                        flag = false;
                }
                if (flag) return true;
            }
        }
        return false;
    }

    /**
     * Проверяет наличие подстроки в виде String
     */
    public boolean containsString(String subString) {
        char[] subArray = new char[subString.length()];
        for (int i = 0; i < subString.length(); i++) {
            subArray[i] = subString.charAt(i);
        }
        return containsString(subArray);
    }

    /**
     * Удаляет из строки ведущие пробельные символы, если они есть
     */
    public void trimString() {
        int i = 0, k = 0;
        while (this.string[i++] == ' ')
            k++;
        if (k > 0) {
            char[] stringNew = new char[lengthString() - k];
            for (int j = 0; j < stringNew.length; j++) {
                stringNew[j] = this.string[j + k];
            }
            this.string = stringNew;
        }
    }

    /**
     * Разворачивает строку
     */
    public void reverseString() {
        char[] stringNew = new char[lengthString()];
        for (int i = 0; i < stringNew.length; i++) {
            stringNew[i] = this.string[lengthString() - i - 1];
        }
        this.string = stringNew;
    }
}
