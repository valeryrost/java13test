package homework.part3_1.task6;

public class Main {
    public static void main(String[] args) {
        char[] test1 = {'t', 'e', 's', 't', '1'};
        AmazingString string1 = new AmazingString(test1);
        AmazingString string2 = new AmazingString("   test2 ttz");

        string1.printString();
        string2.printString();
        System.out.println("Длина строки 1: " + string1.lengthString());
        System.out.println("Длина строки 2: " + string2.lengthString());
        System.out.println("Первый символ строки 1: " + string1.symbolAt(0));
        System.out.println("Последний символ строки 2: " + string2.symbolAt(string2.lengthString() - 1));

        char[] subString1 = {'e', 's'};
        System.out.println("Подстрока1 есть в строке1: " + string1.containsString(subString1));
        String subString2 = "st";
        System.out.println("Подстрока2 есть в строке2: " + string2.containsString(subString2));

        string1.trimString();
        string1.printString();
        string2.trimString();
        string2.printString();

        string1.reverseString();
        string1.printString();
        string2.reverseString();
        string2.printString();
    }
}
