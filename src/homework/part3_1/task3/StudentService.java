package homework.part3_1.task3;
/*
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.
*/

import homework.part3_1.task2.Student;

import java.util.Arrays;

public class StudentService {

    /**
     * Возвращает лучшего студента из массива
     */
    public Student bestStudent(Student[] students) {
        Student best = students[0];
        for (int i = 1; i < students.length; i++) {
            if (students[i - 1].averageScore() < students[i].averageScore())
                best = students[i];
        }
        return best;
    }

    /**
     * Сортирует по фамилии массив студентов
     */
    public void sortBySurname(Student[] students) {
        Student temp;
        for (int j = 0; j < students.length; j++) {
            for (int i = 1; i < students.length; i++) {
                if (students[i - 1].getSurname().compareTo(students[i].getSurname()) > 0) {
                    temp = students[i - 1];
                    students[i - 1] = students[i];
                    students[i] = temp;
                }
            }
        }
    }

}