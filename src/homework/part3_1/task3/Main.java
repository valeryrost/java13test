package homework.part3_1.task3;

import homework.part3_1.task2.Student;

public class Main {
    public static void main(String[] args) {
        Student[] students = new Student[3];

        students[0] = new Student("Иван", "Иванов");
        students[0].addNewGrade(2);
        students[0].addNewGrade(2);
        students[0].addNewGrade(2);
        students[1] = new Student("Степан", "Степанов");
        students[1].addNewGrade(3);
        students[1].addNewGrade(3);
        students[1].addNewGrade(3);
        students[2] = new Student("Михаил", "Михайлов");
        students[2].addNewGrade(5);
        students[2].addNewGrade(5);
        students[2].addNewGrade(5);

        StudentService studentService = new StudentService();
        Student best = studentService.bestStudent(students);
        System.out.println("Лучший студент: " + best.getName() + " " + best.getSurname());

        System.out.println("До сортировки: ");
        for (int i = 0; i < 3; i++) {
            System.out.println(students[i].getSurname());
        }
        studentService.sortBySurname(students);
        System.out.println("После сортировки: ");
        for (int i = 0; i < 3; i++) {
            System.out.println(students[i].getSurname());
        }
    }
}
