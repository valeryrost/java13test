package homework.part3_1.task8;

public class Main {
    public static void main(String[] args) {
        System.out.println("Создано экземпляров Atm: " + Atm.getCounterCreatedAtm());
        Atm atm1 = new Atm(60.74);
        Atm atm2 = new Atm(0.015927);
        System.out.println("Создано экземпляров Atm: " + Atm.getCounterCreatedAtm());

        System.out.println("В 1 банкомате 500 долларов будет: " + atm1.convertDollarsToRubles(500) + " руб.");
        System.out.println("Во 2 банкомате 500 долларов будет: " + atm2.convertDollarsToRubles(500) + " руб.");
        System.out.println("В 1 банкомате 20000 рублей будет: " + atm1.convertRublesDollarsTo(20000) + " $");
        System.out.println("Во 2 банкомате 20000 рублей будет: " + atm2.convertRublesDollarsTo(20000) + " $");
    }
}
