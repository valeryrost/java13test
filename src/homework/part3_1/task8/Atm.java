package homework.part3_1.task8;
/*
Класс должен:
● Содержать конструктор, позволяющий задать курс валют перевода долларов в рубли и курс валют перевода рублей в
доллары (можно выбрать и задать любые положительные значения).
● Содержать два публичных метода, которые позволяют переводить переданную сумму рублей в доллары и долларов в рубли.
● Хранить приватную переменную счетчик — количество созданных инстансов класса Atm и публичный метод, возвращающий
этот счетчик (подсказка: реализуется через static)
 */

public class Atm {
    private static int counterCreatedAtm = 0;
    private double exchangeRateDollarsToRubles;
    private double exchangeRateRublesToDollars;

    public Atm(double exchangeRate) {
        if (exchangeRate < 1) {
            this.exchangeRateRublesToDollars = exchangeRate;
            this.exchangeRateDollarsToRubles = 1 / exchangeRate;
        } else {
            this.exchangeRateDollarsToRubles = exchangeRate;
            this.exchangeRateRublesToDollars = 1 / exchangeRate;
        }
        counterCreatedAtm++;
    }

    /**
     * Возвращает количество созданных экземпляров Atm
     */
    public static int getCounterCreatedAtm() {
        return counterCreatedAtm;
    }

    /**
     * Переводит доллары в рубли
     */
    public double convertDollarsToRubles(double dollars) {
        return (int) (dollars * this.exchangeRateDollarsToRubles * 100) / 100.0;
    }

    /**
     * Переводит рубли в доллары
     */
    public double convertRublesDollarsTo(double rubles) {
        return (int) (rubles * this.exchangeRateRublesToDollars * 100) / 100.0;
    }
}
