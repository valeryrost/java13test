package homework.part3_1.task4;

public class Main {
    public static void main(String[] args) {
        TimeUnit timeUnit1 = new TimeUnit(0);
        TimeUnit timeUnit2 = new TimeUnit(4, 30, 11);
        TimeUnit timeUnit3 = new TimeUnit(12);
        TimeUnit timeUnit4 = new TimeUnit(15, 40);
        TimeUnit timeUnit5 = new TimeUnit(23, 30, 35);
        TimeUnit timeUnit6 = new TimeUnit(25, 65, 65);

        timeUnit1.outputTimeFullFormat();
        timeUnit1.outputTimeTwelveHourFormat();
        timeUnit2.outputTimeFullFormat();
        timeUnit2.outputTimeTwelveHourFormat();
        timeUnit3.outputTimeFullFormat();
        timeUnit3.outputTimeTwelveHourFormat();
        timeUnit4.outputTimeFullFormat();
        timeUnit4.outputTimeTwelveHourFormat();
        timeUnit5.outputTimeFullFormat();
        timeUnit5.outputTimeTwelveHourFormat();
        timeUnit6.outputTimeFullFormat();
        timeUnit6.outputTimeTwelveHourFormat();
        timeUnit6.addTime(1, 1, 1);
        timeUnit6.outputTimeFullFormat();
        timeUnit6.outputTimeTwelveHourFormat();
    }
}
