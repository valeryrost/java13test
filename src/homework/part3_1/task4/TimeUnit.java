package homework.part3_1.task4;
/*
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда должны быть нулевыми.
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда должны быть нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному в
TimeUnit (на вход передаются только часы, минуты и секунды).
*/

public class TimeUnit {
    private int hour;
    private int minute;
    private int second;

    public TimeUnit(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        timeValidate();
    }

    public TimeUnit(int hour, int minute) {
        this(hour, minute, 0);
    }

    public TimeUnit(int hour) {
        this(hour, 0, 0);
    }

    /**
     * Выводим установленное время в формате hh:mm:ss
     */
    public void outputTimeFullFormat() {
        System.out.printf("%02d:%02d:%02d\n", this.hour, this.minute, this.second);
    }

    /**
     * Выводим установленное время в 12-часовом формате
     */
    public void outputTimeTwelveHourFormat() {
        String meridiem;
        int hour12;
        if (this.hour < 12) {
            meridiem = "am";
            hour12 = this.hour;
        } else {
            meridiem = "pm";
            hour12 = this.hour - 12;
        }
        if (hour12 == 0)
            hour12 = 12;
        System.out.printf("%02d:%02d:%02d %s\n", hour12, this.minute, this.second, meridiem);
    }

    /**
     * Прибавляем время к установленному
     */
    public void addTime(int hour, int minute, int second) {
        this.hour += hour;
        this.minute += minute;
        this.second += second;
        timeValidate();
    }

    /**
     * Проверяем корректность времени (секунды и минуты сдвигаем, часы срезаем)
     */
    public void timeValidate() {
        if (this.second >= 60) {
            this.minute += this.second / 60;
            this.second = this.second % 60;
        }
        if (this.minute >= 60) {
            this.hour += this.minute / 60;
            this.minute = this.minute % 60;
        }
        if (this.hour >= 24) {
            this.hour = this.hour % 24;
        }
    }
}
