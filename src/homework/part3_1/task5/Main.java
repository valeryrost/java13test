package homework.part3_1.task5;


public class Main {
    public static void main(String[] args) {
        DayOfWeek[] weekDays = new DayOfWeek[7];
        weekDays[0] = DayOfWeek.MONDAY;
        weekDays[1] = DayOfWeek.TUESDAY;
        weekDays[2] = DayOfWeek.WEDNESDAY;
        weekDays[3] = DayOfWeek.THURSDAY;
        weekDays[4] = DayOfWeek.FRIDAY;
        weekDays[5] = DayOfWeek.SATURDAY;
        weekDays[6] = DayOfWeek.SUNDAY;
        for (int i = 0; i < 7; i++) {
            System.out.println(weekDays[i].dayNumber + " " + weekDays[i].dayName);
        }
    }
}
