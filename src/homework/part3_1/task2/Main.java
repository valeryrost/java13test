package homework.part3_1.task2;


import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("Иван", "Иванов");
        student.addNewGrade(5);
        student.addNewGrade(5);
        student.addNewGrade(4);
        student.addNewGrade(3);
        System.out.println("Список оценок: " + Arrays.toString(student.getGrades()));
        System.out.println("Средний балл: " + student.averageScore());
    }
}
