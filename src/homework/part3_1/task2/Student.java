package homework.part3_1.task2;
/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)
*/

public class Student {
    private String name;
    private String surname;
    private int[] grades;

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.grades = new int[10];
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return this.grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    /**
     * Добавляет новую оценку
     */
    public void addNewGrade(int newGrade) {
        for (int i = 0; i < this.grades.length - 1; i++) {
            this.grades[i] = this.grades[i + 1];
        }
        this.grades[this.grades.length - 1] = newGrade;
    }

    /**
     * Возвращает средний балл студента
     */
    public int averageScore() {
        int sum = 0;
        int i = this.grades.length - 1;
        int j = 0;
        while (this.grades[i] > 0 && i > 0) {
            sum += this.grades[i--];
            j++;
        }
        if (j == 0)
            return 0;
        else
            return sum / j;
    }
}
