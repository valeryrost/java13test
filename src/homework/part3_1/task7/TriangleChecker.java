package homework.part3_1.task7;
/*
Реализовать класс, статический метод которого принимает три длины сторон треугольника и возвращает true,
если возможно составить из них треугольник, иначе false. Входные длины сторон треугольника — числа типа
double. Придумать и написать в методе main несколько тестов для проверки работоспособности класса
(минимум один тест на результат true и один на результат false)
 */

public class TriangleChecker {
    public static void main(String[] args) {
        System.out.println("Возможен треугольник со сторонами 1, 2, 3: " + TriangleChecker.checkTriangle(1, 2, 3));
        System.out.println("Возможен треугольник со сторонами 10, 2, 7: " + TriangleChecker.checkTriangle(10, 2, 7));
        System.out.println("Возможен треугольник со сторонами 15, 2, 15: " + TriangleChecker.checkTriangle(15, 2, 15));
        System.out.println("Возможен треугольник со сторонами 3, 4, 5: " + TriangleChecker.checkTriangle(3, 4, 5));
    }

    public static boolean checkTriangle(double a, double b, double c) {
        return ((a + b) > c) && ((a + c) > b) && ((b + c) > a);
    }
}
