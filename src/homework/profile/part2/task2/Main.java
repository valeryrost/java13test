package homework.profile.part2.task2;

import java.util.*;

/*
С консоли на вход подается две строки s и t. Необходимо вывести true, если
одна строка является валидной анаграммой другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв
другого слова или фразы, обычно с использованием всех исходных букв ровно
один раз.
*/
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        String t = scanner.next();
        System.out.println(checkForAnagram(s, t));
    }

    public static boolean checkForAnagram(String string1, String string2) {
        //вариант решения через Stream
        //return Arrays.equals(string1.chars().sorted().toArray(), string2.chars().sorted().toArray());
        if (string1.length() != string2.length())
            return false;
        int counter;
        Map<Character, Integer> map = new HashMap<>();
        for (Character ch : string1.toCharArray()) {    //Заполняем Map по первой строке
            counter = 1;
            if (map.containsKey(ch)) {
                counter = map.get(ch);
                counter++;
            }
            map.put(ch, counter);
        }
        for (Character ch : string2.toCharArray()) {    //Удаляем из Map по второй строке
            if (!map.containsKey(ch))
                return false;
            counter = map.get(ch);
            if (counter == 1)
                map.remove(ch);
            else {
                counter--;
                map.put(ch, counter);
            }
        }
        return map.size() == 0;
    }
}
