package homework.profile.part2.task3;
/*
Реализовать класс PowerfulSet, в котором должны быть следующие методы:
a. public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает пересечение двух наборов.
Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {1, 2}
b. public <T> Set<T> union(Set<T> set1, Set<T> set2) — Вернуть объединение двух наборов {0, 1, 2, 3, 4}
c. public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
возвращает элементы первого набора без тех, которые находятся также
и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
*/

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {
    private PowerfulSet() {
    }

    /**
     * Возвращает пересечение двух наборов.
     */
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> toReturn = new HashSet<>(set1);
        toReturn.retainAll(set2);
        return toReturn;
    }

    /**
     * Возвращает объединение двух наборов.
     */
    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> toReturn = new HashSet<>(set1);
        toReturn.addAll(set2);
        return toReturn;
    }

    /**
     * Возвращает элементы первого набора без тех, которые находятся также и во втором наборе.
     */
    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> toReturn = new HashSet<>(set1);
        toReturn.removeAll(set2);
        return toReturn;
    }
}
