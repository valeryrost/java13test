package homework.profile.part2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        List<Document> listOfDocuments = new ArrayList<>();
        //добавляем документ с id = 3
        Document document3 = new Document();
        document3.id = 3;
        document3.name = "Doc03";
        document3.pageCount = 15;
        listOfDocuments.add(document3);
        //добавляем документ с id = 2
        Document document2 = new Document();
        document2.id = 2;
        document2.name = "Doc02";
        document2.pageCount = 5;
        listOfDocuments.add(document2);
        //добавляем документ с id = 1
        Document document1 = new Document();
        document1.id = 1;
        document1.name = "Doc01";
        document1.pageCount = 10;
        listOfDocuments.add(document1);
        // печатаем из List
        System.out.println(listOfDocuments.get(0).name + " id= " + listOfDocuments.get(0).id);
        System.out.println(listOfDocuments.get(1).name + " id= " + listOfDocuments.get(1).id);
        System.out.println(listOfDocuments.get(2).name + " id= " + listOfDocuments.get(2).id);
        // переносим в Map
        Map<Integer, Document> mapOfDocuments = organizeDocuments(listOfDocuments);
        // печатаем из Map (по id)
        System.out.println(mapOfDocuments.get(1).name);
        System.out.println(mapOfDocuments.get(2).name);
        System.out.println(mapOfDocuments.get(3).name);
    }

    /**
     * Переносит документы из ArrayList в HashMap
     */
    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> resultMap = new HashMap<>();
        for (Document d : documents) {
            resultMap.put(d.id, d);
        }
        return resultMap;
    }
}
