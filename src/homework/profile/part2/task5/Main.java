package homework.profile.part2.task5;

import java.util.*;

/*
Реализовать метод, который принимает массив words и целое положительное число k.
Необходимо вернуть k наиболее часто встречающихся слов.
Результирующий массив должен быть отсортирован по убыванию частоты
встречаемого слова. В случае одинакового количества частоты для слов, то
отсортировать и выводить их по убыванию в лексикографическом порядке
*/
public class Main {
    public static void main(String[] args) {
        String[] testArray = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"};
        int size = 4;
        String[] resultArray = mostCommonWords(testArray, size);
        for (int i = 0; i < size; i++)
            System.out.print(resultArray[i] + " ");
    }

    /**
     * Возвращает массив k наиболее часто встречающихся слов
     */
    public static String[] mostCommonWords(String[] sourceArray, int k) {
        Map<String, Integer> map = new TreeMap<>();
        String[] resultArray = new String[k];
        int counter;
        // заполняем Map по входящему массиву
        for (String s : sourceArray) {
            counter = 1;
            if (map.containsKey(s)) {
                counter = map.get(s);
                counter++;
            }
            map.put(s, counter);
        }
        int maxValue;
        String maxKey = "";
        // заполняем массив для вывода
        for (int i = 0; i < k; i++) {
            maxValue = Collections.max(map.values());  //находим максимальное значение и ключ
            for (String key : map.keySet()) {
                if (map.get(key) == maxValue)
                    maxKey = key;
            }
            resultArray[i] = maxKey;
            map.remove(maxKey);
        }
        return resultArray;
    }
}
