package homework.profile.part2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции.
*/
public class Main {
    public static void main(String[] args) {
        List<String> list1 = new ArrayList<>();
        list1.add("aaa");
        list1.add("aaa");
        list1.add("bbb");
        list1.add("bbb");
        System.out.println(list1);
        Set<String> set1 = arrayToSet(list1);
        System.out.println(set1);
    }

    public static <T> Set<T> arrayToSet(List<T> sourceArray) {
        return new HashSet<>(sourceArray);
    }
}
