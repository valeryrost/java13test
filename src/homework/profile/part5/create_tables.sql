/*
 Покупатели
*/
create table buyers
(
    id    serial primary key,
    name  varchar(50) NOT NULL,
    phone varchar(30) NOT NULL
);

/*
 Товары
*/
create table products
(
    id    serial primary key,
    name  varchar(50) NOT NULL,
    price integer     NOT NULL
);

/*
 Заказы
*/
create table orders
(
    id         serial primary key,
    buyer_id   integer REFERENCES buyers (id),
    product_id integer REFERENCES products (id),
    quantity   integer NOT NULL,
    order_date timestamp
);
