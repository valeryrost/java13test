--products
INSERT INTO products(name, price)
VALUES ('Розы', 100);
INSERT INTO products(name, price)
VALUES ('Лилии', 50);
INSERT INTO products(name, price)
VALUES ('Ромашки', 25);
select *
from products;
--buyers
INSERT INTO buyers(name, phone)
VALUES ('Иван', '+7(800)500-01-01');
INSERT INTO buyers(name, phone)
VALUES ('Мария', '+7(800)600-55-55');
INSERT INTO buyers(name, phone)
VALUES ('Юрий', '+7(800)700-50-10');
select *
from buyers;
--orders
INSERT INTO orders(buyer_id, product_id, quantity, order_date)
VALUES (1, 1, 15, '2023-01-10');
INSERT INTO orders(buyer_id, product_id, quantity, order_date)
VALUES (1, 2, 17, '2023-01-15');
INSERT INTO orders(buyer_id, product_id, quantity, order_date)
VALUES (2, 3, 21, '2023-01-10');
INSERT INTO orders(buyer_id, product_id, quantity, order_date)
VALUES (3, 1, 25, '2023-01-10');
INSERT INTO orders(buyer_id, product_id, quantity, order_date)
VALUES (3, 2, 27, '2023-01-12');
INSERT INTO orders(buyer_id, product_id, quantity, order_date)
VALUES (3, 3, 55, '2022-12-07');
select *
from orders;
