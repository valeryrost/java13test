--1.По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
select *
from orders o
         join buyers b on b.id = o.buyer_id
         join products p on p.id = o.product_id
where o.id = 2;

--2.Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select *
from orders o
         join buyers b on b.id = o.buyer_id
         join products p on p.id = o.product_id
where b.id = 3
  and o.order_date >= now() - interval '1 month';

--3.Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select name as "Название", quantity as "Количество"
from orders o
         join products p on p.id = o.product_id
order by quantity desc
limit 1;

--4.Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(quantity * p.price) as "Общая выручка"
from orders o
         join buyers b on b.id = o.buyer_id
         join products p on p.id = o.product_id;

