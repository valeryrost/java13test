package homework.profile.part1.task6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //Проверяем имя
        System.out.println("Введите имя: ");
        String str1 = scanner.next();
        try {
            FormValidator.checkName(str1);
        } catch (Exception e1) {
            System.out.println(e1.getMessage());
        }
        System.out.println("Введенное имя- " + str1);
        //Проверяем дату рождения
        System.out.println("Введите дату рождения: ");
        String str2 = scanner.next();
        try {
            FormValidator.checkBirthdate(str2);
        } catch (Exception e2) {
            System.out.println(e2.getMessage());
        }
        System.out.println("Введенная дата- " + str2);
        //Проверяем пол
        System.out.println("Введите пол: ");
        String str3 = scanner.next();
        try {
            FormValidator.checkGender(str3);
        } catch (Exception e3) {
            System.out.println(e3.getMessage());
        }
        System.out.println("Введенный пол- " + str3);
        //Проверяем рост
        System.out.println("Введите рост: ");
        String str4 = scanner.next();
        try {
            FormValidator.checkHeight(str4);
        } catch (Exception e4) {
            System.out.println(e4.getMessage());
        }
        System.out.println("Введенный рост- " + str4);
    }
}
