package homework.profile.part1.task6;
/*
Класс со статическими методами проверки.
На вход всем методам подается String str
*/

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Date;

public class FormValidator {
    private FormValidator() {
    }

    /**
     * Длина имени должна быть от 2 до 20 символов, первая буква заглавная
     */
    public static void checkName(String str) throws Exception {
        if ((str.length() < 2) || (str.length() > 20) || Character.isLowerCase(str.charAt(0)))
            throw new Exception("Имя введено неверно");
    }

    /**
     * Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты.
     */
    public static void checkBirthdate(String str) throws Exception {
        LocalDate date;
        try {
            date = LocalDate.parse(str);
        } catch (DateTimeParseException exception) {
            throw new Exception("Дата введена неверно");
        }
        if (date.isBefore(LocalDate.of(1900, 1, 1)) || date.isAfter(LocalDate.now()))
            throw new Exception("Дата введена неверно");
    }

    /**
     * Пол должен корректно матчится в enum Gender, хранящий Male и Female значения
     */
    public static void checkGender(String str) throws Exception {
        try {
            Gender.valueOf(str);
        } catch (IllegalArgumentException exception) {
            throw new Exception("Пол введен неверно");
        }
    }

    /**
     * Рост должен быть положительным числом и корректно конвертироваться в double
     */
    public static void checkHeight(String str) throws Exception {
        double height = 0;
        try {
            height = Double.parseDouble(str);
        } catch (NumberFormatException exception) {
            throw new Exception("Рост введен неверно");
        }
        if (height <= 0)
            throw new Exception("Рост введен неверно");
    }
}
