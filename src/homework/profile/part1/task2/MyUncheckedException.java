package homework.profile.part1.task2;

/*
Создать собственное исключение MyUncheckedException, являющееся
непроверяемым
 */
public class MyUncheckedException extends RuntimeException {
    public MyUncheckedException(String message) {
        super(message);
    }
}
