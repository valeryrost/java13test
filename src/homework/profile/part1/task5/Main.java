package homework.profile.part1.task5;
/*
Найти и исправить ошибки в следующем коде (сдать исправленный вариант):
*/

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            int n = inputN();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }
        System.out.println("Успешный ввод!");
    }

    private static int inputN() throws Exception {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n <= 0 || n >= 100) {
            throw new Exception("Неверный ввод");
        }
        return n;
    }
}