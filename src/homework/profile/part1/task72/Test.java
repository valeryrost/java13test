package homework.profile.part1.task72;

import java.util.Scanner;

/*
На вход подается число n, массив целых чисел отсортированных по
возрастанию длины n и число p. Необходимо найти индекс элемента массива
равного p. Все числа в массиве уникальны. Если искомый элемент не найден,
вывести -1.
Решить задачу за логарифмическую сложность.
*/
public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        int p = scanner.nextInt();
        int index = -1;
        int low = 0;
        int high = n - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (array[mid] < p)
                low = mid + 1;
            else if (array[mid] > p)
                high = mid - 1;
            else if (array[mid] == p) {
                index = mid;
                break;
            }
        }
        System.out.println(index);
    }
}
