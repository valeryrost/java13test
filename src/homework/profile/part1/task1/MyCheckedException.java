package homework.profile.part1.task1;

/*
Создать собственное исключение MyCheckedException, являющееся
проверяемым.
*/
public class MyCheckedException extends ReflectiveOperationException {
    public MyCheckedException(String message) {
        super(message);
    }
}
