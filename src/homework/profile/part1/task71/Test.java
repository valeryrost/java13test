package homework.profile.part1.task71;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/*
На вход подается число n и массив целых чисел длины n.
Вывести два максимальных числа в этой последовательности
*/
public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Integer[] array = new Integer[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }


        Arrays.sort(array, Collections.reverseOrder());
        System.out.println(array[0] + " " + array[1]);

    }
}
