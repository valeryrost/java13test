package homework.profile.part1.task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

public class FileReadWriteUpper {
    private static final String INPUT_FILE_NAME = "input.txt";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    private static final String PKG_DIRECTORY = "G:\\BITBUCKET\\One\\ProjectOne\\src\\homework\\profile\\part1\\task3";

    public static void readAndWriteUpper() throws IOException {
        Scanner scanner = new Scanner(new File(PKG_DIRECTORY + "\\" + INPUT_FILE_NAME));
        Writer writer = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        try (scanner; writer){
            while (scanner.hasNext()) {
                writer.write(scanner.nextLine().toUpperCase() + "\n");
            }
        }
    }

    public static void main(String[] args) {
        try {
            readAndWriteUpper();
        } catch (IOException e) {
            System.out.println("FileReadWriteError: " + e.getMessage());
        }
    }
}
