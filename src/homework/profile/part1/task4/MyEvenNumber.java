package homework.profile.part1.task4;

/*
Создать класс MyEvenNumber, который хранит четное число int n. Используя
исключения, запретить создание инстанса MyEvenNumber с нечетным числом
*/
public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) throws Exception {
        if (n % 2 != 0)
            throw new Exception("an even number is required");
        this.n = n;
    }

    public int getN() {
        return n;
    }
}
