package homework.profile.part1.task4;

public class Main {
    public static void main(String[] args) {
        try {
            MyEvenNumber num1 = new MyEvenNumber(10);
            System.out.println(num1.getN());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            MyEvenNumber num2 = new MyEvenNumber(11);
            System.out.println(num2.getN());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
