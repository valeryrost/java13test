package homework.profile.part4.task1;
/*
 Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на экран.
*/

import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Integer result = Stream.iterate(1, x -> x + 1)
                .limit(100)
                .filter(x -> (x % 2) == 0)
                .reduce(0, Integer::sum);
        System.out.println("Result: " + result);
    }
}
