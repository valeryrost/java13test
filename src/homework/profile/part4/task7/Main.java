package homework.profile.part4.task7;

import java.util.Set;
import java.util.stream.Collectors;
/*
На вход подается две строки.
Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех возможных операций:
1. Добавить символ.  2. Удалить символ.  3. Заменить символ.
Пример:
“cat” “cats” -> true
“cat” “cut” -> true
“cat” “nut” -> false
*/

public class Main {
    public static void main(String[] args) {
        System.out.println("cat cats - " + checkTwoStrings("cat", "cats"));
        System.out.println("cat cut - " + checkTwoStrings("cat", "cut"));
        System.out.println("cat nut - " + checkTwoStrings("cat", "nut"));
    }

    public static boolean checkTwoStrings(String string1, String string2) {
        if (Math.abs(string1.length() - string2.length()) > 1)
            return false;
        Set<Character> set1 = string1.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());
        Set<Character> set2 = string2.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());
        set1.removeAll(set2);
        return set1.size() < 2;
    }
}
