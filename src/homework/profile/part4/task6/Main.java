package homework.profile.part4.task6;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/*
 Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>
*/
public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> setOfSets = Set.of(Set.of(1, 2), Set.of(2, 3), Set.of(3, 4), Set.of(4, 5));
        Set<Integer> result = setOfSets.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        System.out.println("Result: " + result);
    }
}
