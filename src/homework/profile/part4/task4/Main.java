package homework.profile.part4.task4;

/*
 На вход подается список вещественных чисел. Необходимо отсортировать их по убыванию.
*/

import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Double> list = List.of(3.2, 5.5, 6.0, 2.6, 8.8);
        List<Double> result = list.stream()
                .sorted(Collections.reverseOrder())
                .toList();
        System.out.println("Result: " + result);
    }
}
