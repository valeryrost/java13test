package homework.profile.part3.task6;

import java.util.ArrayDeque;
import java.util.Deque;
/*
Дана строка, состоящая из символов “(“, “)”, “{”, “}”, “[“, “]”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.
Условия для правильной скобочной последовательности те, же что и в задаче 1,
но дополнительно:
● Открывающие скобки должны быть закрыты однотипными закрывающими скобками.
● Каждой закрывающей скобке соответствует открывающая скобка того же типа.
*/

public class Main {
    public static void main(String[] args) {
        String string1 = "{()[]()}";
        String string2 = "{)(}";
        String string3 = "[}";
        String string4 = "[{(){}}][()]{}";
        System.out.println("" + " - " + isCorrectBracketSequence(""));
        System.out.println(string1 + " - " + isCorrectBracketSequence(string1));
        System.out.println(string2 + " - " + isCorrectBracketSequence(string2));
        System.out.println(string3 + " - " + isCorrectBracketSequence(string3));
        System.out.println(string4 + " - " + isCorrectBracketSequence(string4));
    }

    /**
     * Проверяет строку - правильная скобочная последовательность
     */
    public static boolean isCorrectBracketSequence(String sequence) {
        if (sequence.equals(""))
            return true;
        Deque<Character> stack = new ArrayDeque<>();
        char[] toCharSeq = sequence.toCharArray();
        for (int i = 0; i < sequence.length(); i++) {
            switch (toCharSeq[i]) {
                case '(' -> stack.push('(');
                case '{' -> stack.push('{');
                case '[' -> stack.push('[');
                case ')' -> {
                    if (stack.isEmpty() || stack.pop() != '(')
                        return false;
                }
                case '}' -> {
                    if (stack.isEmpty() || stack.pop() != '{')
                        return false;
                }
                case ']' -> {
                    if (stack.isEmpty() || stack.pop() != '[')
                        return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
