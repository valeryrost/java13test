package homework.profile.part3.task4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Написать метод, который с помощью рефлексии получит все интерфейсы
класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.
*/

public class Main {
    public static void main(String[] args) {
        List<Class<?>> allInterfaces = getAllInterfaces(ClassB.class);
        for (Class<?> clazz : allInterfaces) {
            System.out.println(clazz.getSimpleName());
        }
    }

    /**
     * Возвращает список интерфейсов класса, интерфейсов классов-родителей и интерфейсов-родителей
     */
    public static List<Class<?>> getAllInterfaces(Class<?> clazz) {
        List<Class<?>> result = new ArrayList<>();
        while (clazz != Object.class) {            //берем интерфейсы классов
            result.addAll(Arrays.asList(clazz.getInterfaces()));
            clazz = clazz.getSuperclass();
        }
        List<Class<?>> resultAdd = new ArrayList<>();
        for (Class<?> clz : result) {              //проверяем интерфейсы-родители
            if (clz.getInterfaces().length > 0)
                resultAdd.addAll(Arrays.asList(clz.getInterfaces()));
        }
        result.addAll(resultAdd);
        return result;
    }
}
