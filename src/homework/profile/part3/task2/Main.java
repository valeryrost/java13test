package homework.profile.part3.task2;

import homework.profile.part3.task1.IsLike;

/*
Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
любом переданном классе и выведет значение, хранящееся в аннотации, на экран.
*/

public class Main {
    public static void main(String[] args) {
        checkForAnnotations(MyTestClass.class);
    }

    /**
     * Проверяет наличие аннотации @IsLike
     */
    public static void checkForAnnotations(Class<?> clazz) {
        if (!clazz.isAnnotationPresent(IsLike.class)) {
            return;
        }
        IsLike isLike = clazz.getAnnotation(IsLike.class);
        System.out.println("booleanValue= " + isLike.booleanValue());
    }
}
