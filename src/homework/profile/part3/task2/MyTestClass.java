package homework.profile.part3.task2;

import homework.profile.part3.task1.IsLike;

/*
 Тестовый класс с аннотацией @IsLike и booleanValue = false
*/
@IsLike(booleanValue = false)
public class MyTestClass {
    public MyTestClass() {
    }
}
