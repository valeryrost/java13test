package homework.profile.part3.task5;

import java.util.ArrayDeque;
import java.util.Deque;

/*
Дана строка, состоящая из символов “(“ и “)”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.
*/

public class Main {
    public static void main(String[] args) {
        String string1 = "(()()())";
        String string2 = ")(";
        String string3 = "(()";
        String string4 = "((()))";
        System.out.println("" + " - " + isCorrectBracketSequence(""));
        System.out.println(string1 + " - " + isCorrectBracketSequence(string1));
        System.out.println(string2 + " - " + isCorrectBracketSequence(string2));
        System.out.println(string3 + " - " + isCorrectBracketSequence(string3));
        System.out.println(string4 + " - " + isCorrectBracketSequence(string4));
    }

    /**
     * Проверяет строку - правильная скобочная последовательность
     */
    public static boolean isCorrectBracketSequence(String sequence) {
        if (sequence.equals(""))
            return true;
        Deque<Character> stack = new ArrayDeque<>();
        char[] toCharSeq = sequence.toCharArray();
        for (int i = 0; i < sequence.length(); i++) {
            if (toCharSeq[i] == '(') {
                stack.push('(');
            }
            if (toCharSeq[i] == ')') {
                if (stack.isEmpty()) {
                    return false;
                }
                stack.pop();
            }
        }
        return stack.isEmpty();
    }
}
