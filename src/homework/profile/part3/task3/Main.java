package homework.profile.part3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
Есть класс APrinter. С помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки
*/
public class Main {
    public static void main(String[] args) {
        Class<APrinter> printer = APrinter.class;
        try {
            Method method = printer.getDeclaredMethod("print", int.class);
            System.out.print(method.getName() + " ");
            method.invoke(new APrinter(), 200);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException |
                 IllegalArgumentException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
