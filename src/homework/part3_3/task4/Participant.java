package homework.part3_3.task4;
/*
Участник
*/

public class Participant {
    private String nameOfParticipant;
    private Dog dogOfParticipant;
    private double averageRatingOfParticipant;

    public Participant(String nameOfParticipant) {
        this.nameOfParticipant = nameOfParticipant;
        this.dogOfParticipant = null;
        this.averageRatingOfParticipant = 0;
    }

    public String getNameOfParticipant() {
        return nameOfParticipant;
    }

    public Dog getDogOfParticipant() {
        return dogOfParticipant;
    }

    public void setDogOfParticipant(Dog dogOfParticipant) {
        this.dogOfParticipant = dogOfParticipant;
    }

    public Double getAverageRatingOfParticipant() {
        return averageRatingOfParticipant;
    }

    public void setAverageRatingOfParticipant(double averageRatingOfParticipant) {
        this.averageRatingOfParticipant = averageRatingOfParticipant;
    }
}
