package homework.part3_3.task4;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double sum;
        List<Participant> participantList = new ArrayList<>();

        for (int i = 0; i < n; i++)
            participantList.add(new Participant(scanner.next()));
        for (int i = 0; i < n; i++)
            participantList.get(i).setDogOfParticipant(new Dog(scanner.next()));
        for (int i = 0; i < n; i++) {
            sum = 0;
            for (int j = 0; j < 3; j++) {
                sum += scanner.nextDouble();
            }
            participantList.get(i).setAverageRatingOfParticipant((int) (sum / 3 * 10) / 10.0);
        }

        participantList.sort(new Comparator<Participant>() {
            @Override
            public int compare(Participant o1, Participant o2) {
                return o2.getAverageRatingOfParticipant().compareTo(o1.getAverageRatingOfParticipant());
            }
        });

        for (int i = 0; i < 3; i++) {
            System.out.println(participantList.get(i).getNameOfParticipant() + ": " +
                    participantList.get(i).getDogOfParticipant().getNickNameDog() + ", " +
                    participantList.get(i).getAverageRatingOfParticipant());
        }
    }
}

