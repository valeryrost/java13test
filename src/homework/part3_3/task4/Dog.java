package homework.part3_3.task4;

/*
Собака
*/

public class Dog {
    private String nickNameDog;

    public Dog(String nickNameDog) {
        this.nickNameDog = nickNameDog;
    }

    public String getNickNameDog() {
        return nickNameDog;
    }
}
