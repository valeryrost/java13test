package homework.part3_3.task1;

/*
Птицы
*/

public class Bird extends Animal {
    public Bird() {
    }

    @Override
    void wayOfBirth() {
        System.out.println("Птицы откладывают яйца.");
    }
}
