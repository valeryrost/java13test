package homework.part3_3.task1;

/*
Летучая мышь
*/

public class Bat
        extends Mammal
        implements Flying {
    public Bat() {
    }

    @Override
    public void flightSpeed() {
        System.out.println("Летучая мышь летает медленно.");
    }
}
