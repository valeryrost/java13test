package homework.part3_3.task1;

/*
Орел
*/

public class Eagle
        extends Bird
        implements Flying {
    public Eagle() {
    }

    @Override
    public void flightSpeed() {
        System.out.println("Орел летает быстро.");
    }
}
