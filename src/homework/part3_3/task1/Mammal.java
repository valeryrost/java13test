package homework.part3_3.task1;

/*
Млекопитающие
*/

public class Mammal extends Animal {
    public Mammal() {
    }

    @Override
    void wayOfBirth() {
        System.out.println("Млекопитающие живородящие.");
    }
}
