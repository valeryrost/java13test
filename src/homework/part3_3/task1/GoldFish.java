package homework.part3_3.task1;

/*
Золотая рыбка
*/

public class GoldFish
        extends Fish implements Swimming {
    public GoldFish() {
    }

    @Override
    public void swimmingSpeed() {
        System.out.println("Золотая рыбка плавает медленно.");
    }
}
