package homework.part3_3.task1;

/*
Дельфин
*/

public class Dolphin
        extends Mammal
        implements Swimming {
    public Dolphin() {
    }

    @Override
    public void swimmingSpeed() {
        System.out.println("Дельфин плавает быстро.");
    }
}
