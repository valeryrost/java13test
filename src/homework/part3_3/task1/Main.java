package homework.part3_3.task1;

public class Main {
    public static void main(String[] args) {
        Bat bat = new Bat();
        Eagle eagle = new Eagle();
        Dolphin dolphin = new Dolphin();

        Animal goldFish = new GoldFish();

        bat.abilityToEat();
        bat.abilityToSleep();
        bat.wayOfBirth();
        bat.flightSpeed();

        eagle.flightSpeed();
        dolphin.swimmingSpeed();

        goldFish.abilityToEat();
        goldFish.abilityToSleep();
        goldFish.wayOfBirth();
        ((GoldFish) goldFish).swimmingSpeed();

    }
}
