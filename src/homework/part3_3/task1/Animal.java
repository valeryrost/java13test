package homework.part3_3.task1;

/*
Базовый класс всех животных
*/

public abstract class Animal {
    public Animal() {
    }

    public final void abilityToSleep() {
        System.out.println("Все животные спят одинаково.");
    }

    public final void abilityToEat() {
        System.out.println("Все животные едят одинаково.");
    }

    abstract void wayOfBirth();
}
