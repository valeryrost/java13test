package homework.part3_3.task2;

public class Main {
    public static void main(String[] args) {

        Furniture stool = new Stool();
        Furniture table = new Table();
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();

        if (bestCarpenterEver.isWeCanFixIt(stool))
            System.out.println("Мы можем это починить.");
        else
            System.out.println("Это мы чинить не можем.");

        if (bestCarpenterEver.isWeCanFixIt(table))
            System.out.println("Мы можем это починить.");
        else
            System.out.println("Это мы чинить не можем.");
    }
}
