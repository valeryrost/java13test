package homework.part3_3.task2;

/*
Цех по ремонту табуреток
*/

public class BestCarpenterEver {
    public BestCarpenterEver() {
    }

    /**
     * Проверяем экземпляр на входе (табуретка-ли?)
     */
    public boolean isWeCanFixIt(Furniture furniture) {
        if (furniture instanceof Stool)
            return true;
        else
            return false;
    }
}
