package homework.part3_3.task3;
/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Необходимо вывести матрицу на экран, каждый элемент
которого состоит из суммы индекса столбца и строки этого же элемента. Решить
необходимо используя ArrayList.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();   //количество столбцов
        int m = scanner.nextInt();   //количество строк
        ArrayList<ArrayList<Integer>> array = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < m; i++) {
            array.add(new ArrayList<Integer>());
            for (int j = 0; j < n; j++) {
                array.get(i).add(j, i + j);
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(array.get(i).get(j) + " ");
            }
            System.out.println();
        }
    }
}
