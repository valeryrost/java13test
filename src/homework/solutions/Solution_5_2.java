package homework.solutions;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем в
последующих двух строках подаются координаты X (номер столбца) и Y (номер
строки) точек, которые задают прямоугольник.
Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
заполненной нулями и вывести всю матрицу на экран.
*/

import java.util.Scanner;

public class Solution_5_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = 0;
            }
        }
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();
        for (int i = x1; i <= x2; i++) {
            array[y1][i] = 1;
            array[y2][i] = 1;
        }
        for (int i = y1; i <= y2; i++) {
            array[i][x1] = 1;
            array[i][x2] = 1;
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n - 1)
                    System.out.print(array[i][j]);
                else
                    System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
