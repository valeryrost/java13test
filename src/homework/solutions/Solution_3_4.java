package homework.solutions;

/*
Дано натуральное число n. Вывести его цифры в “столбик”.
 */

import java.util.Scanner;

public class Solution_3_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        if (0 < n && n < 1000000) {
            int num = 0;
            int i = 0;
            int[] massNum = new int[6];
            while (n > 0) {
                num = n % 10;
                n = n / 10;
                massNum[i] = num;
                i++;
            }
            for (int j = i - 1; j >= 0; j--)
                System.out.println(massNum[j]);
        } else
            System.out.println("0 < n < 1000000");
    }
}
