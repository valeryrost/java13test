package homework.solutions;
/*
На вход подаются числа A — недельная норма белков, B — недельная норма
жиров, C — недельная норма углеводов и K — недельная норма калорий.
Затем передаются 7 строк, в которых в том же порядке указаны сколько было
съедено Петей нутриентов в каждый день недели. Если за неделю в сумме по
каждому нутриенту не превышена недельная норма, то вывести “Отлично”,
иначе вывести “Нужно есть поменьше”.
*/

import java.util.Scanner;

public class Solution_5_6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int k = scanner.nextInt();
        int[][] ateWeek = new int[7][4];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                ateWeek[i][j] = scanner.nextInt();
            }
        }
        int sumA = sumWeek(ateWeek, 0);
        int sumB = sumWeek(ateWeek, 1);
        int sumC = sumWeek(ateWeek, 2);
        int sumK = sumWeek(ateWeek, 3);
        if (sumA <= a && sumB <= b && sumC <= c && sumK <= k)
            System.out.println("Отлично");
        else
            System.out.println("Нужно есть поменьше");
    }

    public static int sumWeek(int[][] array, int column) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i][column];
        }
        return sum;
    }
}
