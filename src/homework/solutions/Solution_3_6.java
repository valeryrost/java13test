package homework.solutions;
/*
В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
число n - количество денег для размена. Необходимо найти минимальное
количество купюр с помощью которых можно разменять это количество денег
(соблюсти порядок: первым числом вывести количество купюр номиналом 8,
вторым - 4 и т д)
 */

import java.util.Scanner;

public class Solution_3_6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int sum = input.nextInt();
        int q8 = 0;
        int q4 = 0;
        int q2 = 0;
        int q1 = 0;
        q8 = sum / 8;
        sum = sum % 8;
        q4 = sum / 4;
        sum = sum % 4;
        q2 = sum / 2;
        sum = sum % 2;
        q1 = sum;
        System.out.println(q8 + " " + q4 + " " + q2 + " " + q1);
    }
}
