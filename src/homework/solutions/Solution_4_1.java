package homework.solutions;
/*
На вход подается число N — длина массива. Затем передается массив
вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный
массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран
 */

import java.util.Scanner;

public class Solution_4_1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        double[] m = new double[n];
        for (int i = 0; i < n; i++) {
            m[i] = input.nextDouble();
        }
        System.out.println(arithmeticMean(m));
    }

    public static double arithmeticMean(double[] array) {
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum / array.length;
    }
}
