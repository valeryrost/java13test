package homework.solutions;
/*
Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
Ему хочется уметь разделять строку по первому пробелу. Для этого он может
воспользоваться методами indexOf() и substring().
На вход подается строка. Нужно вывести две строки, полученные из входной
разделением по первому пробелу.
 */

import java.util.Scanner;

public class Solution_2_7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String stringSource = input.nextLine();
        int i = stringSource.indexOf(' ');
        String string1 = stringSource.substring(0, i);
        String string2 = stringSource.substring(i + 1, stringSource.length());
        System.out.println(string1);
        System.out.println(string2);
    }
}
