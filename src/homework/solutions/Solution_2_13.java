package homework.solutions;
/*
нас есть почтовая посылка (String mailPackage). Каждая почтовая
посылка проходит через руки проверяющего. Работа проверяющего
заключается в следующем:
● во-первых, посмотреть не пустая ли посылка;
● во-вторых, проверить нет ли в ней камней или запрещенной продукции.
Наличие камней или запрещенной продукции указывается в самой посылке в конце
или в начале. Если в посылке есть камни, то будет написано слово "камни!", если
запрещенная продукция, то будет фраза "запрещенная продукция".
После осмотра посылки проверяющий должен сказать:
● "камни в посылке" – если в посылке есть камни;
● "в посылке запрещенная продукция" – если в посылке есть что-то запрещенное;
● "в посылке камни и запрещенная продукция" – если в посылке находятся камни
и запрещенная продукция;
● "все ок" – если с посылкой все хорошо.
Если посылка пустая, то с посылкой все хорошо.
 */

import java.util.Scanner;

public class Solution_2_13 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String mailPackage = input.nextLine();
        boolean isEmty = mailPackage.length() == 0;
        boolean isPresentStone = mailPackage.contains("камни!");
        boolean isPresentProhibited = mailPackage.contains("запрещенная продукция");
        if (isEmty)
            System.out.println("все ок");
        else {
            if (isPresentStone) {
                if (isPresentProhibited) {
                    System.out.println("в посылке камни и запрещенная продукция");
                } else
                    System.out.println("камни в посылке");
            }
            if (isPresentProhibited && !isPresentStone)
                System.out.println("в посылке запрещенная продукция");
            if (!isPresentStone && !isPresentProhibited)
                System.out.println("все ок");
        }
    }
}
