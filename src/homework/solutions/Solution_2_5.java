package homework.solutions;
/*
Дома дочери Пети опять нужна помощь с математикой! В этот раз ей
нужно проверить, имеет ли предложенное квадратное уравнение решение или
нет.
На вход подаются три числа — коэффициенты квадратного уравнения a, b, c.
Нужно вывести "Решение есть", если оно есть и "Решения нет", если нет.
 */

import java.util.Scanner;

public class Solution_2_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        int d = (int) (Math.pow(b, 2) - 4 * a * c);
        if (d < 0)
            System.out.println("Решения нет");
        else
            System.out.println("Решение есть");
    }
}
