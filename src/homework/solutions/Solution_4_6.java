package homework.solutions;
/*
На вход подается строка S, состоящая только из русских заглавных
букв (без Ё).
Необходимо реализовать метод, который кодирует переданную строку с
помощью азбуки Морзе и затем вывести результат на экран. Отделять коды букв
нужно пробелом.
 */

import java.util.Scanner;

public class Solution_4_6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        String[] morseCode = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---",
                "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....",
                "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
        for (int i = 0; i < s.length(); i++) {
            int index = Math.abs('А' - s.charAt(i));
            System.out.print(morseCode[index] + " ");
        }
    }
}

