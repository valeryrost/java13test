package homework.solutions;
/*
На вход подается число N. Необходимо вывести цифры числа справа налево.
Решить задачу нужно через рекурсию.
*/

import java.util.Scanner;

public class Solution_5_10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        numLeftRight(n);
    }

    static void numLeftRight(int n) {
        if (n > 0) {
            System.out.print(n % 10 + " ");
            numLeftRight(n / 10);
        }
    }
}
