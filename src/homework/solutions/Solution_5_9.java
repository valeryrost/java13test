package homework.solutions;
/*
На вход подается число N. Необходимо вывести цифры числа слева направо.
Решить задачу нужно через рекурсию.
*/

import java.util.Scanner;

public class Solution_5_9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        numLeftRight(n);
    }

    static void numLeftRight(int n) {
        if (n > 0) {
            numLeftRight(n / 10);
            System.out.print(n % 10 + " ");
        }
    }
}
