package homework.solutions;
/*
Даны положительные натуральные числа m и n. Найти остаток от деления m на
n, не выполняя операцию взятия остатка
 */

import java.util.Scanner;

public class Solution_3_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        int res = m / n;
        int ost = m - res * n;
        System.out.println(ost);
    }
}
