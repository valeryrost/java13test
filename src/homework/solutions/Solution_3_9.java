package homework.solutions;
/*
На вход последовательно подается возрастающая последовательность из n
целых чисел, которая может начинаться с отрицательного числа.
Посчитать и вывести на экран, какое количество отрицательных чисел было
введено в начале последовательности. Помимо этого нужно прекратить
выполнение цикла при получении первого неотрицательного числа на вход.
 */

import java.util.Scanner;

public class Solution_3_9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int counterNegNum = 0;
        int n = 0;
        int ai = 0;
        do {
            n++;
            ai = input.nextInt();
            if (ai < 0)
                counterNegNum++;
        } while (n < 1000 && ai < 0);
        System.out.println(counterNegNum);
    }
}
