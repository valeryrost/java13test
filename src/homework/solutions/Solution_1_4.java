package homework.solutions;

import java.util.Scanner;

public class Solution_1_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int count = input.nextInt();
        int totalMinutes = count / 60;
        int currentMinute = totalMinutes % 60;
        int totalHours = totalMinutes / 60;
        int currentHour = totalHours % 24;
        System.out.println(currentHour + " " + currentMinute);

    }
}
