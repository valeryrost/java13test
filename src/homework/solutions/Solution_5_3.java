package homework.solutions;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передаются координаты X и Y расположения коня на шахматной доске.
Необходимо заполнить матрицу размера NxN нулями, местоположение коня
отметить символом K, а позиции, которые он может бить, символом X.
*/

import java.util.Scanner;

public class Solution_5_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        char[][] array = new char[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = '0';
            }
        }
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        array[y][x] = 'K';
        if (x - 2 >= 0 && y - 1 >= 0)    //налево + вверх
            array[y - 1][x - 2] = 'X';
        if (x - 2 >= 0 && y + 1 < n)     //налево + вниз
            array[y + 1][x - 2] = 'X';
        if (x + 2 < n && y - 1 >= 0)     //направо + вверх
            array[y - 1][x + 2] = 'X';
        if (x + 2 < n && y + 1 < n)      //направо + вниз
            array[y + 1][x + 2] = 'X';
        if (y - 2 >= 0 && x - 1 >= 0)    //вверх + налево
            array[y - 2][x - 1] = 'X';
        if (y - 2 >= 0 && x + 1 < n)     //вверх + направо
            array[y - 2][x + 1] = 'X';
        if (y + 2 < n && x - 1 >= 0)     //вниз + налево
            array[y + 2][x - 1] = 'X';
        if (y + 2 < n && x + 1 < n)      //вниз + направо
            array[y + 2][x + 1] = 'X';
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n - 1)
                    System.out.print(array[i][j]);
                else
                    System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
