package homework.solutions;
/*
На вход подается два положительных числа m и n. Найти сумму чисел между m
и n включительно.
 */

import java.util.Scanner;

public class Solution_3_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        int res = 0;
        for (int i = m; i <= n; i++) {
            res += i;
        }
        System.out.println(res);
    }
}
