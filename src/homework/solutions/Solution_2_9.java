package homework.solutions;
/*
Пока Петя практиковался в работе со строками, к нему подбежала его
дочь и спросила: "А правда ли, что тригонометрическое тождество (sin^2(x)+
cos^2(x) - 1 == 0) всегда-всегда выполняется?"
Напишите программу, которая проверяет, что при любом x на входе
тригонометрическое тождество будет выполняться (то есть будет выводить true
при любом x).
 */

import java.util.Scanner;

public class Solution_2_9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x = input.nextInt();
        boolean b = (int) (Math.pow(Math.sin(x), 2) + Math.pow(Math.cos(x), 2)) == 1;
        System.out.println(b);
    }
}
