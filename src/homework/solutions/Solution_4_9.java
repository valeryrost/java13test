package homework.solutions;
/*
На вход подается число N — длина массива. Затем передается массив
строк из N элементов (разделение через перевод строки). Каждая строка
содержит только строчные символы латинского алфавита.
Необходимо найти и вывести дубликат на экран. Гарантируется что он есть и
только один.
 */

import java.util.Scanner;

public class Solution_4_9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] array = new String[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.next();
        }
        String duplicate = array[0];
        for (int i = 1; i < n; i++) {
            if (duplicate.equals(array[i]))
                break;
            duplicate = array[i];
        }
        System.out.println(duplicate);
    }
}
