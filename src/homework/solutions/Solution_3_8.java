package homework.solutions;
/*
На вход подается:
○ целое число n,
○ целое число p
○ целые числа a1, a2 , … an
Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго
больше p.
 */

import java.util.Scanner;

public class Solution_3_8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int p = input.nextInt();
        int sum = 0;
        int[] m = new int[n];
        for (int i = 0; i < n; i++) {
            m[i] = input.nextInt();
            if (m[i] > p)
                sum += m[i];
        }
        System.out.println(sum);
    }
}
