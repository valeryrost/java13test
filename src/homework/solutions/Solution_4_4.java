package homework.solutions;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо вывести на экран построчно сколько встретилось различных
элементов. Каждая строка должна содержать количество элементов и сам
элемент через пробел.
 */

import java.util.Scanner;

public class Solution_4_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        int i = 0;
        int j = 0;
        while (i < n) {
            j = 1;
            if (i < n - 1) {
                while (array[i] == array[i + 1]) {
                    j++;
                    i++;
                    if (i == n - 1)
                        break;
                }
            }
            System.out.print(j + " ");
            System.out.println(array[i++]);
        }
    }
}
