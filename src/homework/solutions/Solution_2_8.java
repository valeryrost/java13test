package homework.solutions;
/*
Раз так легко получается разделять по первому пробелу, Петя решил
немного изменить предыдущую программу и теперь разделять строку по
последнему пробелу
 */

import java.util.Scanner;

public class Solution_2_8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String stringSource = input.nextLine();
        int i = stringSource.lastIndexOf(' ');
        String string1 = stringSource.substring(0, i);
        String string2 = stringSource.substring(i + 1, stringSource.length());
        System.out.println(string1);
        System.out.println(string2);
    }
}
