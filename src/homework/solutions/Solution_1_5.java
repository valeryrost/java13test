package homework.solutions;

import java.util.Scanner;

public class Solution_1_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int inInches = input.nextInt();
        double inCentimeters = inInches * 2.54;
        System.out.println(inCentimeters);

    }
}
