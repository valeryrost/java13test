package homework.solutions;
/*
Старый телефон Андрея сломался, поэтому он решил приобрести
новый. Продавец телефонов предлагает разные варианты, но Андрея
интересуют только модели серии samsung или iphone. Также Андрей решил
рассматривать телефоны только от 50000 до 120000 рублей. Чтобы не тратить
время на разговоры, Андрей хочет написать программу, которая поможет ему
сделать выбор.
На вход подается строка – модель телефона и число – стоимость телефона.
Нужно вывести "Можно купить", если модель содержит слово samsung или
iphone и стоимость от 50000 до 120000 рублей включительно. Иначе вывести
"Не подходит".
Гарантируется, что в модели телефона не указано одновременно несколько
серий.
 */

import java.util.Scanner;

public class Solution_2_14 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String phoneModel = input.nextLine();
        int phoneCost = input.nextInt();
        boolean modelMatch = phoneModel.contains("samsung") || phoneModel.contains("iphone");
        boolean costMatch = (50000 <= phoneCost) && (phoneCost <= 120000);
        if (modelMatch && costMatch)
            System.out.println("Можно купить");
        else
            System.out.println("Не подходит");
    }
}
