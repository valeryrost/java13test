package homework.solutions;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передается сама матрица, состоящая из натуральных чисел. После этого
передается натуральное число P.
Необходимо найти элемент P в матрице и удалить столбец и строку его
содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.
*/

import java.util.Scanner;

public class Solution_5_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = n - 1;
        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();
        int x = 0;
        int y = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (array[i][j] == p) {
                    y = i;
                    x = j;
                }
            }
        }
        int[][] arrayNew = new int[m][m];
        int i1 = 0;
        int j1 = 0;
        for (int i = 0; i < n; i++) {
            if (i != x) {
                j1 = 0;
                for (int j = 0; j < n; j++) {
                    if (j != y)
                        arrayNew[j1++][i1] = array[j][i];
                }
                i1++;
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                if (j == m - 1)
                    System.out.print(arrayNew[i][j]);
                else
                    System.out.print(arrayNew[i][j] + " ");
            }
            System.out.println();
        }
    }
}
