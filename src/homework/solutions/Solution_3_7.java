package homework.solutions;
/*
Дана строка s. Вычислить количество символов в ней, не считая пробелов
(необходимо использовать цикл).
 */

import java.util.Scanner;

public class Solution_3_7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        int countSymbols = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ')
                countSymbols++;
        }
        System.out.println(countSymbols);
    }
}
