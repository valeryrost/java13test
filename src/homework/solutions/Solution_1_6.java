package homework.solutions;

import java.util.Scanner;

public class Solution_1_6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int count = input.nextInt();
        double inMmiles = count / 1.60934;
        System.out.println(inMmiles);
    }
}
