package homework.solutions;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого аналогично передается второй
массив (aj) длины M.
Необходимо вывести на экран true, если два массива одинаковы (то есть
содержат одинаковое количество элементов и для каждого i == j элемент ai ==
aj). Иначе вывести false.
 */

import java.util.Scanner;

public class Solution_4_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] m1 = new int[n];
        for (int i = 0; i < n; i++) {
            m1[i] = input.nextInt();
        }
        int m = input.nextInt();
        int[] m2 = new int[m];
        for (int i = 0; i < m; i++) {
            m2[i] = input.nextInt();
        }
        System.out.println(arrayEquality(m1, m2));
    }

    public static boolean arrayEquality(int[] m1, int[] m2) {
        if (m1.length != m2.length) return false;
        for (int i = 0; i < m1.length; i++) {
            if (m1[i] != m2[i])
                return false;
        }
        return true;
    }
}
