package homework.solutions;
/*
На вход подается число N — количество участников конкурса. Затем в N
строках переданы имена хозяев. После этого в N строках переданы клички
собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
оценки судей. Победителями являются три участника, набравшие
максимальное среднее арифметическое по оценкам 3 судей. Необходимо
вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
Гарантируется, что среднее арифметическое для всех участников будет
различным.
*/

import java.util.Scanner;

public class Solution_5_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();   //количество участников конкурса
        String[] namesOwners = new String[n];
        for (int i = 0; i < n; i++) {
            namesOwners[i] = scanner.next();
        }

        String[] namesDogs = new String[n];
        for (int i = 0; i < n; i++) {
            namesDogs[i] = scanner.next();
        }
        //оценки судей
        double[][] scores = new double[3][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                scores[j][i] = scanner.nextDouble();
            }
        }
        //таблица средних значений
        double[] average = new double[n];
        for (int i = 0; i < n; i++) {
            double sum = 0;
            for (int j = 0; j < 3; j++) {
                sum += scores[j][i];
            }
            average[i] = (int) (sum / 3 * 10) / 10.0;
        }
        //таблица - индекс и среднее
        Object[][] tableComp = new Object[n][2];
        for (int i = 0; i < n; i++) {
            tableComp[i][0] = i;
            tableComp[i][1] = average[i];
        }
        //сортировка по среднему
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - 1; j++) {
                double first = (Double) tableComp[j][1];
                double second = (Double) tableComp[j + 1][1];
                if (first < second)
                    swap(tableComp, j, j + 1);
            }
        }
        //вывод
        for (int i = 0; i < 3; i++) {
            printIndex((Integer) tableComp[i][0], namesOwners, namesDogs, average);
        }
    }

    /**
     * Печать в нужном формате
     */
    static void printIndex(int index, String[] nOwners, String[] nDogs, double[] av) {
        System.out.println(nOwners[index] + ": " + nDogs[index] + ", " + av[index]);
    }

    /**
     * Обмен строк с индексами i и j местами в двумерном массиве array
     */
    static void swap(Object[][] array, int i, int j) {
        for (int k = 0; k < array[i].length; k++) {
            Object buff = array[i][k];
            array[i][k] = array[j][k];
            array[j][k] = buff;
        }
    }
}
