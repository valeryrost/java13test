package homework.solutions;
/*
Вывести на экран “ёлочку” из символа звездочки (*) заданной высоты N. На N +
1 строке у “ёлочки” должен быть отображен ствол из символа |
 */

import java.util.Scanner;

public class Solution_3_10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int numOfStars = 1;
        int numOfSpace = n - 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= numOfSpace; j++)
                System.out.print(" ");
            for (int k = 1; k <= numOfStars; k++)
                System.out.print("#");
            numOfStars += 2;
            numOfSpace--;
            System.out.println();
        }
        for (int i = 1; i < n; i++)
            System.out.print(" ");
        System.out.println("|");
    }
}
