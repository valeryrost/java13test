package homework.solutions;
/*
"А логарифмическое?" - не унималась дочь.
Напишите программу, которая проверяет, что log(e^n) == n для любого
вещественного n.
 */

import java.util.Scanner;

public class Solution_2_10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double n = input.nextDouble();
        boolean b = (int) (Math.log(Math.pow(Math.E, n)) - n) == 0;
        System.out.println(b);
    }
}
