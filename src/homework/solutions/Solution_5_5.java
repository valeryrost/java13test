package homework.solutions;
/*
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.
Необходимо вывести true, если она является симметричной относительно
побочной диагонали, false иначе.
Побочной диагональю называется диагональ, проходящая из верхнего правого
угла в левый нижний.
*/

import java.util.Scanner;

public class Solution_5_5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
        boolean flag = true;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (array[i][j] != array[n - 1 - j][n - 1 - i]) {
                    flag = false;
                    break;
                }
            }
            if (!flag)
                break;
        }
        System.out.println(flag);
    }
}
