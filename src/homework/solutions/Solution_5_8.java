package homework.solutions;
/*
На вход подается число N. Необходимо посчитать и вывести на экран сумму его
цифр. Решить задачу нужно через рекурсию.
*/

import java.util.Scanner;

public class Solution_5_8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(sumNumbers(n));
    }

    static int sumNumbers(int n) {
        if (n / 10 != 0)
            return n % 10 + sumNumbers(n / 10);
        else
            return n % 10;
    }
}
