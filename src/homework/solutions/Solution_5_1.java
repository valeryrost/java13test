package homework.solutions;
/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Затем сам передается двумерный массив, состоящий из
натуральных чисел.
Необходимо сохранить в одномерном массиве и вывести на экран
минимальный элемент каждой строки.
 */

import java.util.Scanner;

public class Solution_5_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(); //столбцы
        int m = scanner.nextInt(); //строки
        int[][] array = new int[n][m];
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                array[i][j] = scanner.nextInt();
            }
        }
        int[] res = new int[m];
        for (int j = 0; j < m; j++) {
            int min = array[0][j];
            for (int i = 1; i < n; i++) {
                if (min > array[i][j]) {
                    min = array[i][j];
                }
            }
            res[j] = min;
        }
        for (int i = 0; i < m; i++) {
            System.out.print(res[i] + " ");
        }
    }
}
