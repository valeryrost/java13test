package homework.solutions;

import java.util.Locale;
import java.util.Scanner;

public class Solution_1_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in).useLocale(Locale.US);
        int r = sc.nextInt();
        double v = 4/3.0 * Math.PI * Math.pow(r, 3);
        System.out.println(v);
    }
}



