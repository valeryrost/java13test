package homework.solutions;
/*
На вход подается два положительных числа m и n. Необходимо вычислить m^1
+ m^2 + ... + m^n
 */

import java.util.Scanner;

public class Solution_3_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        int res = 0;
        for (int i = 1; i <= n; i++) {
            res += Math.pow(m, i);
        }
        System.out.println(res);
    }
}
