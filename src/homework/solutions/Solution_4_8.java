package homework.solutions;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M.
Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
для которого |ai - M| минимальное). Если их несколько, то вывести
максимальное число.
 */

import java.util.Scanner;

public class Solution_4_8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int min = Math.abs(array[0] - m);
        int minIndex = 0;
        for (int i = 1; i < n; i++) {
            int temp = Math.abs(array[i] - m);
            if (temp < min) {
                min = temp;
                minIndex = i;
            } else if ((temp == min) && (array[i] > array[minIndex])) {
                min = temp;
                minIndex = i;
            }
        }
        System.out.println(array[minIndex]);
    }
}
