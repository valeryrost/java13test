package homework.solutions;
/*
За каждый год работы Петя получает на ревью оценку. На вход
подаются оценки Пети за последние три года (три целых положительных числа).
Если последовательность оценок строго монотонно убывает, то вывести "Петя,
пора трудиться"
В остальных случаях вывести "Петя молодец!"
 */

import java.util.Scanner;

public class Solution_2_1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        if (a > b && b > c)
            System.out.println("Петя, пора трудиться");
        else
            System.out.println("Петя молодец!");
    }
}
