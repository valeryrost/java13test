package homework.part3_2;
//Класс библиотека

import java.util.ArrayList;
import java.util.List;

public class Library {

    private List<Book> listExistingBooksInLibrary = new ArrayList<>();
    private int visitorsCounter;

    public Library() {
        this.visitorsCounter = 0;
    }


    /**
     * Добавляет новую книгу в библиотеку, если книги с таким наименованием еще нет
     */
    public void addNewBookToLibrary(Book newBook) {
        if (findBookInLibrary(newBook.getTitleOfBook()) == null)
            this.listExistingBooksInLibrary.add(newBook);
        else
            System.out.println("Книга с таким названием уже есть");
    }

    /**
     * Удаляет книгу из библиотеки по названию, если такая книга есть и не одолжена
     */
    public boolean deleteBookFromLibrary(String lookingTitleOfBook) {
        Book deletedBook = findBookInLibrary(lookingTitleOfBook);
        if (deletedBook == null) {
            System.out.println("Книга не найдена");
            return false;
        }
        if (deletedBook.getBookReader() != null) {
            System.out.println("Книга сейчас одолжена");
            return false;
        }
        this.listExistingBooksInLibrary.remove(deletedBook);
        System.out.println("Книга удалена");
        return true;

    }

    /**
     * Находит и возвращает книгу по названию, либо null если такой нет
     */
    public Book findBookInLibrary(String lookingTitleOfBook) {
        for (Book book : this.listExistingBooksInLibrary) {
            if (book.getTitleOfBook().equals(lookingTitleOfBook))
                return book;
        }
        return null;
    }

    /**
     * Находит и возвращает список книг по автору
     */
    public ArrayList findBooksByAuthor(String lookingAuthor) {
        List<Book> listBooksByAuthor = new ArrayList<>();
        for (Book book : this.listExistingBooksInLibrary)
            if (book.getAuthorOfBook().equals(lookingAuthor))
                listBooksByAuthor.add(book);
        return (ArrayList) listBooksByAuthor;
    }

    /**
     * Даём книгу посетителю по названию, если: 1.она есть в библиотеке
     * 2.у посетителя сейчас нет книги 3. Она не одолжена.
     */
    public boolean giveBookToVisitor(Visitor visitor, String lookingTitleOfBook) {
        if (visitor.getBookBeingRead() != null) {
            System.out.println("Посетитель уже получил книгу");
            return false;
        }
        Book book = findBookInLibrary(lookingTitleOfBook);
        if (book == null) {
            System.out.println("Книга с таким названием отсутствует");
            return false;
        }
        if (book.getBookReader() != null) {
            System.out.println("Книга уже одолжена");
            return false;
        }
        book.setBookReader(visitor);
        visitor.setBookBeingRead(book);
        System.out.println("Книга выдана успешно");
        if (visitor.getVisitorId() == 0) {
            visitorsCounter++;
            visitor.setVisitorId(visitorsCounter);
        }
        return true;
    }

    /**
     * Возвращаем книгу в библиотеку от посетителя, который ранее одалживал книгу. Не принимаем книгу
     * от другого посетителя. Книга перестает считаться одолженной. У посетителя не остается книги.
     * Вариант без оценки
     */
    public boolean returnBookFromVisitor(Visitor visitor) {
        if (visitor.getVisitorId() == 0) {
            System.out.println("Посетитель не оформлен в библиотеке");
            return false;
        }
        if (visitor.getBookBeingRead() == null) {
            System.out.println("У посетителя нет выданных книг");
            return false;
        }
        visitor.getBookBeingRead().setBookReader(null);
        visitor.setBookBeingRead(null);
        System.out.println("Книга возвращена в библиотеку");
        return true;
    }

    /**
     * Возвращаем книгу в библиотеку от посетителя, который ранее одалживал книгу. Не принимаем книгу
     * от другого посетителя. Книга перестает считаться одолженной. У посетителя не остается книги.
     * Вариант с оценкой
     */
    public boolean returnBookFromVisitor(Visitor visitor, int valueRate) {
        Book book = visitor.getBookBeingRead();
        if (returnBookFromVisitor(visitor)) {
            book.addRateToList(Rate.ofNumber(valueRate));
            System.out.println("Книге добавлена оценка: " + valueRate);
            return true;
        }
        return false;
    }

    /**
     * Возвращает оценку книги по её наименованию
     */
    public Double getRateOfBook(String titleOfBook) {
        return findBookInLibrary(titleOfBook).getAverageRateOfBook();
    }

    /**
     * Печатает библиотеку
     */
    public void printLibrary() {
        for (Book book : this.listExistingBooksInLibrary)
            System.out.println(book);
    }

    public int getVisitorsCounter() {
        return visitorsCounter;
    }
}
