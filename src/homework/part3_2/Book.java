package homework.part3_2;
//Класс книга

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Book {
    private String titleOfBook;      //Название книги
    private String authorOfBook;     //Автор книги
    private Visitor bookReader;      //Текущий читатель
    private List<Rate> listOfRates;  //Список оценок

    public Book(String titleOfBook, String authorOfBook) {
        this.titleOfBook = titleOfBook;
        this.authorOfBook = authorOfBook;
        this.bookReader = null;
        this.listOfRates = new ArrayList<>();
    }

    /**
     * Возвращает среднее арифметическое оценок
     */
    public Double getAverageRateOfBook() {
        if (this.listOfRates.isEmpty())
            return 0.0;
        int sum = 0;
        for (Rate rate : this.listOfRates)
            sum += rate.valueRate;
        return (int) ((double) sum / this.listOfRates.size() * 10) / 10.0;
    }

    /**
     * Добавляет оценку в список
     */
    public void addRateToList(Rate rate) {
        this.listOfRates.add(rate);
    }

    public String getTitleOfBook() {
        return titleOfBook;
    }

    public void setTitleOfBook(String titleOfBook) {
        this.titleOfBook = titleOfBook;
    }

    public String getAuthorOfBook() {
        return authorOfBook;
    }

    public void setAuthorOfBook(String authorOfBook) {
        this.authorOfBook = authorOfBook;
    }

    public Visitor getBookReader() {
        return bookReader;
    }

    public void setBookReader(Visitor bookReader) {
        this.bookReader = bookReader;
    }

    @Override
    public String toString() {
        return "Book{" + "titleOfBook='" + titleOfBook + '\'' + ", authorOfBook='" + authorOfBook + '\'' + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return titleOfBook.equals(book.titleOfBook) && authorOfBook.equals(book.authorOfBook);
    }

    @Override
    public int hashCode() {
        return Objects.hash(titleOfBook, authorOfBook);
    }
}
