package homework.part3_2;

import java.util.List;

//Тест
public class Main {
    public static void main(String[] args) {
        //создаём библиотеку
        Library library = new Library();
        //создаём посетителей
        Visitor visitor1 = new Visitor("Иванов");
        Visitor visitor2 = new Visitor("Петров");
        Visitor visitor3 = new Visitor("Сидоров");
        //заполняем библиотеку
        fillTestLibrary(library);
        //выводим список книг
        library.printLibrary();
        //удаляем существующую книгу
        library.deleteBookFromLibrary("Название001");
        //выводим список книг
        library.printLibrary();
        //пробуем удалить книгу, с неправильным названием
        library.deleteBookFromLibrary("Название00001");
        //пробуем добавить книгу, с существующим названием
        library.addNewBookToLibrary(new Book("Название002", "Автор1"));
        //пробуем выдать книгу, с неправильным названием
        library.giveBookToVisitor(visitor1, "Название00001");
        //пробуем выдать книгу, с правильным названием
        library.giveBookToVisitor(visitor1, "Название002");
        //пробуем её удалить
        library.deleteBookFromLibrary("Название002");
        //пробуем выдать вторую книгу тому-же посетителю
        library.giveBookToVisitor(visitor1, "Название003");
        //пробуем выдать уже выданную книгу
        library.giveBookToVisitor(visitor2, "Название002");
        //даём книгу второму посетителю
        library.giveBookToVisitor(visitor2, "Название003");
        System.out.println("Посетителей, оформленных в библиотеке: " + library.getVisitorsCounter());
        //пытаемся принять книгу от неоформленного посетителя
        library.returnBookFromVisitor(visitor3);
        //принимаем обратно книгу
        library.returnBookFromVisitor(visitor1);
        //берем список книг Автора1 и выводим на экран
        List<Book> testListByAuthor;
        testListByAuthor = library.findBooksByAuthor("Автор1");
        for (Book book : testListByAuthor)
            System.out.println(book);
        //Проверяем систему оценок книг посетителями
        System.out.println("Рейтинг у Название005: " + library.getRateOfBook("Название005"));
        library.giveBookToVisitor(visitor3,"Название005");
        //возвращаем книгу с оценкой 5
        library.returnBookFromVisitor(visitor3,5);
        System.out.println("Рейтинг у Название005: " + library.getRateOfBook("Название005"));
        library.giveBookToVisitor(visitor3,"Название005");
        //возвращаем книгу с оценкой 4
        library.returnBookFromVisitor(visitor3,4);
        System.out.println("Рейтинг у Название005: " + library.getRateOfBook("Название005"));
        library.giveBookToVisitor(visitor3,"Название005");
        //возвращаем книгу с оценкой 2
        library.returnBookFromVisitor(visitor3,2);
        System.out.println("Рейтинг у Название005: " + library.getRateOfBook("Название005"));

    }

    public static void fillTestLibrary(Library library) {
        library.addNewBookToLibrary(new Book("Название001", "Автор1"));
        library.addNewBookToLibrary(new Book("Название002", "Автор1"));
        library.addNewBookToLibrary(new Book("Название003", "Автор1"));
        library.addNewBookToLibrary(new Book("Название004", "Автор1"));
        library.addNewBookToLibrary(new Book("Название005", "Автор2"));
        library.addNewBookToLibrary(new Book("Название006", "Автор2"));
        library.addNewBookToLibrary(new Book("Название007", "Автор2"));
        library.addNewBookToLibrary(new Book("Название008", "Автор2"));
        library.addNewBookToLibrary(new Book("Название009", "Автор2"));
        library.addNewBookToLibrary(new Book("Название010", "Автор3"));
        library.addNewBookToLibrary(new Book("Название011", "Автор3"));
        library.addNewBookToLibrary(new Book("Название012", "Автор3"));
        library.addNewBookToLibrary(new Book("Название013", "Автор3"));
        library.addNewBookToLibrary(new Book("Название014", "Автор1"));
        library.addNewBookToLibrary(new Book("Название015", "Автор2"));
        library.addNewBookToLibrary(new Book("Название016", "Автор3"));
        library.addNewBookToLibrary(new Book("Название017", "Автор4"));
        library.addNewBookToLibrary(new Book("Название018", "Автор4"));
        library.addNewBookToLibrary(new Book("Название019", "Автор1"));
        library.addNewBookToLibrary(new Book("Название020", "Автор2"));
    }
}
