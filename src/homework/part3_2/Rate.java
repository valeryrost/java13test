package homework.part3_2;

/*
Класс оценок
*/
public enum Rate {
    EXCELLENT(5, "Отлично"),
    GOOD(4, "Хорошо"),
    SATISFACTORY(3, "Удовлетворительно"),
    UNSATISFACTORY(2, "Неудовлетворительно"),
    BAD(1, "Плохо");

    public final int valueRate;
    public final String nameRate;
    private static final Rate[] ALL = values();

    Rate(int valueRate, String nameRate) {
        this.valueRate = valueRate;
        this.nameRate = nameRate;
    }

    public static Rate ofNumber(int valueRate) {
        for (Rate rate : ALL) {
            if (rate.valueRate == valueRate)
                return rate;
        }
        return null;
    }
}
