package homework.part3_2;
//Класс посетитель
public class Visitor {
    private String visitorsName;   //Имя посетителя
    private Book bookBeingRead;    //Книга, которую он читает
    private int visitorId;         //Номер, присвоенный в библиотеке

    public Visitor(String visitorsName) {
        this.visitorsName = visitorsName;
        this.bookBeingRead = null;
        this.visitorId = 0;
    }

    public String getVisitorsName() {
        return visitorsName;
    }

    public Book getBookBeingRead() {
        return bookBeingRead;
    }

    public void setBookBeingRead(Book bookBeingRead) {
        this.bookBeingRead = bookBeingRead;
    }

    public int getVisitorId() {
        return visitorId;
    }

    public void setVisitorId(int visitorId) {
        this.visitorId = visitorId;
    }
}
