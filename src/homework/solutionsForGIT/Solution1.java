package homework.solutionsForGIT;
/*
Необходимо реализовать игру. Алгоритм игры должен быть записан в
отдельном методе. В методе main должен быть только вызов метода с
алгоритмом игры.
Условия следующие:
Компьютер «загадывает» (с помощью генератора случайных чисел) целое
число M в промежутке от 0 до 1000 включительно. Затем предлагает
пользователю угадать это число. Пользователь вводит число с клавиатуры.
Если пользователь угадал число M, то вывести на экран "Победа!". Если
введенное пользователем число меньше M, то вывести на экран "Это число
меньше загаданного." Если введенное число больше, то вывести "Это число
больше загаданного." Продолжать игру до тех пор, пока число не будет отгадано
или пока не будет введено любое отрицательное число.
*/

import java.util.Random;
import java.util.Scanner;

public class Solution1 {
    static Scanner scanner = new Scanner(System.in);
    static Random generator = new Random();

    public static void main(String[] args) {
        playGame();
    }

    /**
     * Запускает игру
     */
    static void playGame() {
        int n;
        int m = generator.nextInt(1001);
        System.out.println("Введите число от 0 до 1000 (либо отрицательное для выхода)");
        do {
            n = scanner.nextInt();
            if (n < 0)
                return;
            if (n == m)
                System.out.println("Победа!");
            else if (n < m)
                System.out.println("Это число меньше загаданного.");
            else
                System.out.println("Это число больше загаданного.");
        }
        while (n != m);
    }
}
