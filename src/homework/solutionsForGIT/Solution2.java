package homework.solutionsForGIT;
/*
Создать программу генерирующую пароль.
На вход подается число N — длина желаемого пароля. Необходимо проверить,
что N >= 8, иначе вывести на экран "Пароль с N количеством символов
небезопасен" (подставить вместо N число) и предложить пользователю еще раз
ввести число N.
Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
вывести его на экран. В пароле должны быть:
● заглавные латинские символы
● строчные латинские символы
● числа
● специальные знаки(_*-)
 */

import java.util.Random;

import java.util.Scanner;

public class Solution2 {
    static Random generator = new Random();
    static String numbers = "0123456789";
    static String signs = "_*-";
    static String upperCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static String lowerCharacters = "abcdefghijklmnopqrstuvwxyz";
    static String allCharacters = numbers + signs + upperCharacters + lowerCharacters;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;
        do {
            System.out.println("Введите длину пароля N:");
            n = scanner.nextInt();
            if (n < 8)
                System.out.println("Пароль с " + n + " количеством символов небезопасен");
        } while (n < 8);
        char[] password = new char[n];
        password[0] = generateUppercaseCharacter();
        password[1] = generateLowercaseCharacter();
        password[2] = generateNumber();
        password[3] = generateSpecial();
        for (int i = 4; i < n; i++) {
            password[i] = generateAnyCharacter();
        }
        System.out.println(password);
    }

    /**
     * Возвращает цифру
     */
    static char generateNumber() {
        return numbers.charAt(generator.nextInt(numbers.length()));
    }

    /**
     * Возвращает спецсимвол
     */
    static char generateSpecial() {
        return signs.charAt(generator.nextInt(signs.length()));
    }

    /**
     * Возвращает заглавную букву
     */
    static char generateUppercaseCharacter() {
        return upperCharacters.charAt(generator.nextInt(upperCharacters.length()));
    }

    /**
     * Возвращает строчную букву
     */
    static char generateLowercaseCharacter() {
        return lowerCharacters.charAt(generator.nextInt(lowerCharacters.length()));
    }

    /**
     * Возвращает любой символ
     */
    static char generateAnyCharacter() {
        return allCharacters.charAt(generator.nextInt(allCharacters.length()));
    }
}
