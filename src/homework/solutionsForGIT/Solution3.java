package homework.solutionsForGIT;
/*
Решить задачу (7) за линейное время:
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
 */

import java.util.Scanner;


public class Solution3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arraySource = new int[n];            //Исходный массив
        for (int i = 0; i < n; i++) {
            arraySource[i] = scanner.nextInt();
        }
        int[] arrayDestination = new int[n];      //Итоговый массив
        int[] arrayNegative = new int[n];         //Массив на основе отрицательных элементов (обратная сортировка)
        int[] arrayPositive = new int[n];         //Массив на основе положительных элементов (прямая сортировка)
        int indexN = 0;
        while (arraySource[indexN] < 0) {
            arrayNegative[indexN] = arraySource[indexN] * arraySource[indexN];
            indexN++;
        }
        int indexP = indexN;
        while (indexP < n) {
            arrayPositive[indexP] = arraySource[indexP] * arraySource[indexP];
            indexP++;
        }
        int index1 = indexN - 1;
        int index2 = indexN;
        int index3 = 0;
        while (index1 >= 0 && index2 < n) {     //Проходим оба массива
            arrayDestination[index3++] = arrayNegative[index1] < arrayPositive[index2] ? arrayNegative[index1--] : arrayPositive[index2++];
        }
        while (index1 >= 0) {                   //Оставшиеся элементы первого массива
            arrayDestination[index3++] = arrayNegative[index1--];
        }
        while (index2 < n) {                    //Оставшиеся элементы второго массива
            arrayDestination[index3++] = arrayPositive[index2++];
        }
        for (int i = 0; i < n; i++) {           //Выводим итоговый сортированный массив
            System.out.print(arrayDestination[i] + " ");
        }
    }
}
