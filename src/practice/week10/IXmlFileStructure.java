package practice.week10;

import practice.week10.xmltags.XmlFirstTag;

public interface IXmlFileStructure {

    XmlFirstTag fillFirstTag();

    void fillSecondTag();

    void fillNTag();
}
