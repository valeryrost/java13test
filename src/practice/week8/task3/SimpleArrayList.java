package practice.week8.task3;
/*
Примитивная реализация ArrayList.
Массив только int, из методов только добавлять элемент,
получать size и увеличивать капасити, когда добавляется новый.
*/

import java.util.Arrays;

public class SimpleArrayList {
    //количество элементов
    private int size;
    private int[] arr;
    //объем массива
    private int capacity;
    private static final int DEFAULT_CAPACITY = 5;
    private int currIndex;

    public SimpleArrayList() {
        arr = new int[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
        currIndex = 0;
    }

    public SimpleArrayList(int size) {
        arr = new int[size];
        capacity = size;
        this.size = 0;
        currIndex = 0;
    }

    /*
    Добавляет новый элемент в список(массив). При достижении размера внутреннего
    массива происходит его увеличение в два раза.
    */
    public void add(int elem) {
        if (currIndex >= capacity) {
            capacity = 2 * capacity;
            arr = Arrays.copyOf(arr, capacity);
            //System.arraycopy()
        }
        arr[currIndex] = elem;
        size++;
        currIndex++;
    }

    //Возможная реализация метода удаления из MyArrayList
    public void remove(int idx) {
        for (int i = idx; i < currIndex; i++) {
            arr[i] = arr[i + 1];
        }
        arr[currIndex] = -1;
        currIndex--;
        size--;
    }

    //достает элемент по индексу
    public int get(int idx) {
        //убрать =, потому что сначала присваивает, а потом увеличивает,
        // если равно на 1 выходить будем за пределы

        //будет ошибка (индекс последнего элемента у нас 0, хотя его нет вообще)
        if (idx < 0 || idx >= size) {
            //выбросить исключение (UnsupportedOperationException)
            System.out.println("Невозможно взять элемент по заданному индексу: " + idx);
            return -1;
        } else {
            return arr[idx];
        }
    }

    public int size() {
        return size;
    }
}
