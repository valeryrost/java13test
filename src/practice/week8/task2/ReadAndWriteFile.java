package practice.week8.task2;

import java.io.*;
import java.util.Scanner;

public class ReadAndWriteFile {
    public static final String FOLDER_DIRECTORY = "G:\\BITBUCKET\\One\\ProjectOne\\src\\week8\\oop2\\task2\\file";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    //утильный класс
    private ReadAndWriteFile() {
    }

    public static void readAndWriteData(String filePath) throws IOException {
        Scanner scanner = new Scanner(new File(filePath));
        String[] days = new String[10];
        int i = 0;
        while (scanner.hasNextLine()) {
            days[i++] = scanner.nextLine();
        }

        Writer writer = new FileWriter(FOLDER_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        for (int j = 0; j < i; j++) {
            String res = "Порядковый номер " + days[j] + " = " + WeekDays.ofName(days[j]).dayNumber + "\n";
            writer.write(res);
        }
        writer.close();
        scanner.close();

         //Пример try with resources (Closable интерфейс) -> не надо явно закрывать потоки (ресурсы)
         //        try (Writer writer1 = new FileWriter("")){
         //            System.out.println();
         //        }

    }

    public static void readAndWriteData() throws IOException {
        readAndWriteData(FOLDER_DIRECTORY + "\\input.txt");
    }
}
