package practice.week4;
/*
Даны числа m < 13 и n < 7.
Вывести все степени (от 0 до n включительно) числа m с помощью цикла.
3 6
->
1
3
9
27
81
243
729
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        //----
        for (int i = 0; i <= n; i++) {
            System.out.println((int) (Math.pow(m, i)));
        }
        //------
        int res = 1;
        System.out.println(res);
        for (int i = 1; i <= n; i++) {
            res = res * m;
            System.out.println(res);
        }
        //-------
        int res1 = 1;
        int j = 1;

        System.out.println(res1);
        while (j <= n) {
            res1 *= m;
            System.out.println(res1);
            j++;
        }

    }
}
