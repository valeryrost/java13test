package practice.week4;
/*
Начальный вклад в банке равен 1000.
Каждый месяц размер вклада увеличивается на P процентов от имеющейся суммы (0 < P < 25).
Найти через какое количество времени размер вклада будет больше 1100
и вывести  найденное количество месяцев и итоговой размер вклада.

15 ->
1
1150.0

3 ->
4
1125.50881
 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int p = scanner.nextInt();
        double deposit = 1000;
        int i = 0;
        while (deposit < 1100) {
            deposit = deposit + deposit / 100 * p;
            i++;
        }
        System.out.println(i);
        System.out.println(deposit);
    }
}
