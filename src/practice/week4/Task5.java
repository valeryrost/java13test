package practice.week4;
/*
Дано целое число n, n > 0. Вывести сумму всех цифр этого числа.
92180 -> 20 //9 + 2 + 1 + 8 + 0 == 20
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println("Input " + n);
        int sum = 0;

        while (n > 0) {
            sum = sum + n % 10;
            System.out.println(sum);
            n = n / 10;
            System.out.println(n);
        }
        System.out.println(sum);

        //через преобразование в строку:
        /*
        sum += Integer.parseInt(String.valueOf(num.charAt(i)));
         */


    }

}

