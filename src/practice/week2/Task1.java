package practice.week2;
/*
Дано число n. Нужно проверить четное ли оно.
*/

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число: ");
        int n = scanner.nextInt();
        String str;
/*
        if (n % 2 == 0) {
            str = " четное ";
        } else {
            str = " нечетное ";
        }
        System.out.println(n + str);
*/
        str = (n % 2 == 0) ? " четное " : " нечетное ";
        System.out.println(n + str);






    }
}
