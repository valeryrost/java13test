package practice.week6;

import java.util.Scanner;

/*
Найдем факториал числа n рекурсивно.
 */

public class Task1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

//        int res = 1;
//        for (int i = 1; i <= n; i++) {
//            res = res * i;
//        }

        int res = faktorial(n);
        System.out.println(res);
    }

    public static int faktorial(int n) {
        if (n <= 1)
            return 1;
        return n * faktorial(n - 1);
    }

    public static int faktorialTail(int n, int result) {
        return 1;
    }
}
