package practice.week9.logger;

public abstract class FileNameHandler {
    private String defaultFileName = "default_output";

    public String getDefaultFileName(){
        return defaultFileName;
    }

    public abstract String getExtension();
}

