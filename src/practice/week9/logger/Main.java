package practice.week9.logger;

public class Main {
    public static void main(String[] args) {
        ConsoleLogger consoleLogger = new ConsoleLogger();
        consoleLogger.log("мы в main");

        Logger txtLogger = new TxtFileLogger("G:\\BITBUCKET\\One\\ProjectOne\\src\\week9\\oop3\\logger\\log_bulb1");
        Bulb bulb = new Bulb(txtLogger);

        bulb.turnOn();
        bulb.turnOff();
        bulb.isShining();

        Logger csvLogger = new CsvFileLogger("G:\\BITBUCKET\\One\\ProjectOne\\src\\week9\\oop3\\logger\\log_bulb2");
        Bulb bulb1 = new Bulb(csvLogger);
        //Bulb bulb1 = new Bulb(consoleLogger);

        bulb1.turnOn();
        bulb1.turnOff();
        bulb1.isShining();
    }
}
