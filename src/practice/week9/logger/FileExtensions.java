package practice.week9.logger;


public interface FileExtensions {
    String CSV_FILE_EXTENSION = ".csv";
    String TXT_FILE_EXTENSION = ".txt";
}
