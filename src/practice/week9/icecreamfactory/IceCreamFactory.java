package practice.week9.icecreamfactory;
/*
реализация паттерна Фабрика для нашего мороженного
 */
public class IceCreamFactory {
    private IceCreamFactory() {
    }

    public static IceCream getIceCream(IceCreamType type) {
        IceCream iceCream = null;
        switch (type) {
            case CHERRY -> iceCream = new CherryIceCream();
            case CHOCOLATE -> iceCream = new ChocolateIceCream();
            case VANILLA -> iceCream = new VanillaIceCream();
        }
        return iceCream;
    }

    //НЕ НАДО ТАК
    public static IceCream getIceCream(Object o) {
        IceCream iceCream = null;
        if (o instanceof ChocolateIceCream) {
            iceCream = new ChocolateIceCream();
        }
        if (o instanceof CherryIceCream) {
            iceCream = new CherryIceCream();
        }
        if (o instanceof VanillaIceCream) {
            iceCream = new VanillaIceCream();
        }
        return iceCream;
    }

    //Можно зайти через Class<?> / Class
    public static IceCream getIceCream(Class obj) {
        System.out.println("OBJECT: " + obj);
        IceCream iceCream = null;

        if (obj.equals(ChocolateIceCream.class)) {
            iceCream = new ChocolateIceCream();
        }
        if (obj.equals(CherryIceCream.class)) {
            iceCream = new CherryIceCream();
        }
        if (obj.equals(VanillaIceCream.class)) {
            iceCream = new VanillaIceCream();
        }
        return iceCream;
    }
}

