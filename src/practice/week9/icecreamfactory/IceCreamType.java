package practice.week9.icecreamfactory;

public enum IceCreamType {
    CHERRY,
    CHOCOLATE,
    VANILLA
}
