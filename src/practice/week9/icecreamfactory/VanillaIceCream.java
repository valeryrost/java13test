package practice.week9.icecreamfactory;

public class VanillaIceCream
        implements IceCream {
    @Override
    public void printIngredients() {
        System.out.println("Vanilla, Cream, Ice, Love, Stevia");
    }
}
