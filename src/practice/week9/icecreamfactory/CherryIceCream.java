package practice.week9.icecreamfactory;

public class CherryIceCream
        implements IceCream {
    @Override
    public void printIngredients() {
        System.out.println("Cherry, Cream, Ice, Love");
    }
}

