package practice.week9.icecreamfactory;

public class ChocolateIceCream implements IceCream{
    @Override
    public void printIngredients() {
        System.out.println("Chocolate, Cream, Ice");
    }
}

