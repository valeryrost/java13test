package practice.week1;
/*
        Напишите аналог функции swap, которая меняет значения двух параметров местами (без вспомогательной переменной)
        Входные данные
        a = 8; b = 10
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите a и b :");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println("Введено a = " + a + " b = " + b);
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println("Меняем a = " + a + " b = " + b);

    }
}
