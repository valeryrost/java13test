package practice.week1;
/*
     Перевод литров в галлоны. С консоли считывается число n - количество литров, нужно перевести это число в галлоны.
     (1 литр = 0,219969 галлона)
 */


import java.util.Scanner;

public class Task7 {

    public static final double LITERS_IN_GALLON = 0.219969;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите :");
        int n = scanner.nextInt();
        System.out.println("Введено " + n);
        double res = n * LITERS_IN_GALLON;
        System.out.println("Итог :" + res);



    }
}
