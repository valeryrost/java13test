package practice.week1;
/*
     Даны целые числа a, b и с, определяющие квадратное уравнение. Вычислить дискриминант.
     Подсказка: D = b^2 - 4 * a * c
     Входные данные
     a = 6 b = -28 с = 79
 */


import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите a,b,c:");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        System.out.println("Введено a= " + a + " b= " + " c= " + c);

        int d = b * b - 4 * a * c;

        System.out.println("Итог : " + d);


    }
}
