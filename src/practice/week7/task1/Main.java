package practice.week7.task1;

public class Main {
    public static void main(String[] args) {
        Bulb bulb = new Bulb();
        System.out.println(bulb.isToggle());
        bulb.turnOn();
        System.out.println(bulb.isToggle());

        Chandelier chandelier = new Chandelier(4);
        chandelier.turnOn();


    }
}
