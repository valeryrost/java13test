package practice.week7.task3;

public class Main {
    public static void main(String[] args) {
        System.out.println(FieldValidator.validateEmail("andy_12@bk.ru"));
        System.out.println(FieldValidator.validateDate("11.10.1990"));
        System.out.println(FieldValidator.validatePhone("11.10.1990"));
        System.out.println(FieldValidator.validateName("Andrei"));

    }
}
