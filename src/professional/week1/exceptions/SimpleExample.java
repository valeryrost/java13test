package professional.week1.exceptions;

import java.util.Scanner;

//схема всех исключений в java
//https://javastudy.ru/wp-content/uploads/2016/01/exceptionsInJavaHierarchy.png
//stacktrace
public class SimpleExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
//        if (n ==0) {
//            System.out.println("Деление на 0! Нельзя! ");
//            return;
//        }
        //System.out.println(100 / n);
        try {
            toDivide(100, n);
        } catch (MyArithmeticException e) {
            System.out.println(e.getMessage());
        }
//            throw new MyArithmeticException();
//            System.out.println("Деление на 0! Нельзя! ");
//            System.out.println(e.getMessage());
        //throw new UnsupportedOperationException();
//        }
//        finally {
//            System.out.println("Выход из программы. Наконец-то.");
//        }
    }

    public static void toDivide(int a, int b) throws MyArithmeticException {
        try {
            System.out.println(a / b);
        } catch (Exception e) {
            System.out.println(e.getMessage());
//            throw new MyArithmeticException();
        } finally {
            System.out.println("Выход из программы. Наконец-то.");
        }
    }

}
