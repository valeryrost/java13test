package professional.week2.task1;

public class Pair<T extends String, U extends Number> {
    public T first;
    public U second;

    public void print() {
        System.out.println("First: " + first + "; second: " + second);
    }
}
