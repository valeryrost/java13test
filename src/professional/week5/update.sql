--Обновление данных
--Обновить время добавления для книги с id = 2

select * from books;

update books
set date_added = '2025-02-02'
where id = 2;

rollback;

commit;

select *
from books;
