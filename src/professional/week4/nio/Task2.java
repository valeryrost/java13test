package professional.week4.nio;
/*
Создать папку и подпапку в src/
*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Task2 {
    public static void main(String[] args) {
        Path path1 = Paths.get("src/newFolder");
        Path path2 = Paths.get("src/newFolder/newSubFolder");
        try {
            Files.createDirectory(path1);
            Files.createDirectory(path2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
