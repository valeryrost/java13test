package professional.week4.streams;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 *
 */
public class StreamsExample {
    public static void main(String[] args) {
        List<String> places = List.of("Nepal, Kathmandu", "Nepal, Pokhara", "India, Delhi", "USA, New York", "Africa, Nigeria");
        places.stream()
                .filter((place) -> place.startsWith("Nepal"))
                //.map((place) -> place.toUpperCase())
                .map(String::toUpperCase)
                //выражение- :: является ссылкой на метод, который эквивалентен лямбда-выражению (p) -> p.toUpperCase()
                .sorted()
                .forEach(System.out::println);
        //.forEach((place) -> System.out.println(place));
        //выражение System.out::println является ссылкой на метод, который эквивалентен лямбда-выражению x -> System.out.println(x);
    }
   // Function<Integer,Integer>
    //Predicate<String>
}

