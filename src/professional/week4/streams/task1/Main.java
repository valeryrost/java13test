package professional.week4.streams.task1;

import professional.week4.functional.task2.Square;

import java.util.ArrayList;
import java.util.List;

/*
 Использовать реализованный функциональный интерфейс Square на массиве чисел, вывести на экран
*/
public class Main {
    public static void main(String[] args) {
        Square s = x -> x * x;
        List<Integer> nums = new ArrayList<>();
        nums.add(3);
        nums.add(5);
        nums.stream()
//              .map(num -> s.calculate(num))
                .map(s::calculate)
                .forEach(System.out::println);
    }
}


