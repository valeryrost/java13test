package professional.week4.functional.task5;

@FunctionalInterface
public interface MyMagicInterface<T> {
    T execute(T param);
}

