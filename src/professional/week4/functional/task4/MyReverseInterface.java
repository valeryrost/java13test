package professional.week4.functional.task4;
@FunctionalInterface
public interface MyReverseInterface {
    String reverseString(String n);
}
