package professional.week4.functional.task4;

/*
реализовать метод, чтобы вывести строку наоборот,
используя наш ReverseInterface
 */
public class Main {
    public static void main(String[] args) {
        MyReverseInterface reverseInterface = (String str) -> new StringBuilder(str).reverse().toString();
        System.out.println(reverseInterface.reverseString("Lambda"));

        MyReverseInterface reverseInterface1 = (str) -> {
            String res = "";
            for (int i = str.length() - 1; i >= 0; i--) {
                res += str.charAt(i);
            }
            return res;
        };
        System.out.println(reverseInterface1.reverseString("Lambda"));
    }
}
