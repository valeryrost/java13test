package professional.consult.profdz1.task4;

/*
 * Создать класс MyEvenNumber, который хранит четное число int n. Используя
 * исключения, запретить создание инстанса с нечетным числом.
 */
public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) throws IllegalArgumentException {
        if (n % 2 == 0) {
            this.n = n;
        }
        else {
            throw new IllegalArgumentException("Wrong number");
        }
    }
}
