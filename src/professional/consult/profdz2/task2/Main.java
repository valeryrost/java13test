package professional.consult.profdz2.task2;

import java.util.*;

public class Main {
    //анаграмма
    //клоака - околка
    //бейсбол - бобслей
    public static void main(String[] args) {
        System.out.println(isAnagram1("клоака", "околка"));
        System.out.println(isAnagram1("бейсбол", "бобслей"));
        System.out.println(isAnagram("бейсбол", "бобслей"));
        System.out.println(isAnagram("бейсбол", "бобслей"));
    }

    public static boolean isAnagramStream(String s, String t) {
        return Arrays.equals(s.chars().sorted().toArray(), t.chars().sorted().toArray());
    }

    private static boolean isAnagram(String s, String t) {

        if (s.length() != t.length()) {
            return false;
        }

        //клоа
        Set<Character> setFirst = new LinkedHashSet<>();
        for (char ch : s.toCharArray()) {
            setFirst.add(ch);
        }
        //окла
        Set<Character> setSecond = new LinkedHashSet<>();
        for (char ch : t.toCharArray()) {
            setSecond.add(ch);
        }

        return setFirst.equals(setSecond);
    }

    //через массивы решение - тоже правильное
//    char[] first = str1.replace(" ", "").toCharArray();
//    char[] secod = str2.replace(" ", "").toCharArray();
//    if (first.length()  != secod.length()){
//        return false;
//    }
//    Arrays.sort(first);
//    Arrays.sort(secod);
//    return Arrays.equals(first, secod);

    //it works!
    public static boolean isAnagram1(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }
        HashMap<Character, Integer> s1 = new HashMap<>();
        for (char element : str1.toLowerCase().toCharArray()) {
            s1.put(element, s1.getOrDefault(element, 0) + 1);
        }
        HashMap<Character, Integer> s2 = new HashMap<>();
        for (char element : str2.toLowerCase().toCharArray()) {
            s2.put(element, s2.getOrDefault(element, 0) + 1);
        }
        return s1.equals(s2);
    }
    //часть задачи три!
    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>(set1);
        set.retainAll(set2);
        return set;
        //return set1.retainAll(set2);
    }
}

