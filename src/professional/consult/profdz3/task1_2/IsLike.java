package professional.consult.profdz3.task1_2;

/*
Создать аннотацию @IsLike, применимую к классу во время выполнения программы.
Аннотация может хранить boolean значение.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface IsLike {
    boolean value() default false;
}
