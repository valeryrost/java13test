package professional.consult.profdz3.task1_2;

/*
Написать метод, который рефлексивно проверит наличие аннотации @IsLike на любом переданном классе
и выведет значение, хранящееся в аннотации, на экран
 */
public class Main {
    public static void main(String[] args) {
        check(LikeClass.class);
        check(NotLikeClass.class);
        check(SimpleClass.class);
    }

    public static void check(Class<?> cls) {
        if (!cls.isAnnotationPresent(IsLike.class)) {
            System.out.println("No isLike annotation");
            return;
        }
        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println("IsLike value: " + isLike.value());
    }


    /*
    Есть класс APrinter:
public class APrinter {
   public void print(int a) {
       System.out.println(a);
   }
}
.invoke()
Class<APrinter> cls => cls.взятьМетод()->вызвать его передав инстанс класса и  значение нашего инта (входного параметра для метода)
     */
}

