package myProbe.abstraction;

import java.math.BigInteger;

public class TestAll {
    public static void main(String[] args) {
        Number x = 3;
        System.out.println(x.intValue());
        System.out.println(x.doubleValue());

        Number x1 = new Integer(3);
        System.out.println(x1.intValue());
        System.out.println(((Integer) x1).compareTo(new Integer(4)));

        String[] cities = {"Севастополь", "Брянск", "Архангельск", "Тамбов"};
        java.util.Arrays.sort(cities);
        for (String city : cities)
            System.out.print(city + " ");
        System.out.println();

        BigInteger[] hugeNumbers = {new BigInteger("2323231092923992"),
                new BigInteger("432232323239292"),
                new BigInteger("54623239292")};
        java.util.Arrays.sort(hugeNumbers);
        for (BigInteger number : hugeNumbers)
            System.out.print(number + " ");

        House house1 = new House(1, 1750.50);
        House house2 = (House)house1.clone();
        System.out.println(house1.equals(house2));
    }
}