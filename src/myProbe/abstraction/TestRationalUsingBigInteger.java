package myProbe.abstraction;

import java.math.BigInteger;
import java.util.Scanner;

public class TestRationalUsingBigInteger {
    public static void main(String[] args) {
        // Получить два рациональных числа
        Scanner input = new Scanner(System.in);
        System.out.print("Введите числитель и знаменатель первого рационального числа через пробел: ");
        String n1 = input.next();
        String d1 = input.next();

        System.out.print("Введите числитель и знаменатель второго рационального числа через пробел: ");
        String n2 = input.next();
        String d2 = input.next();

        RationalUsingBigInteger r1 = new RationalUsingBigInteger(new BigInteger(n1), new BigInteger(d1));
        RationalUsingBigInteger r2 = new RationalUsingBigInteger(new BigInteger(n2), new BigInteger(d2));

        // Отобразить результаты
        System.out.println(r1 + " + " + r2 + " = " + r1.add(r2));
        System.out.println(r1 + " - " + r2 + " = " + r1.subtract(r2));
        System.out.println(r1 + " * " + r2 + " = " + r1.multiply(r2));
        System.out.println(r1 + " / " + r2 + " = " + r1.divide(r2));
        System.out.println(r2 + " равно " + r2.doubleValue());
    }
}
