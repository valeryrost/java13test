package myProbe.generics;

public class AnyWildCardDemo {
    public static void main(String[] args) {
        GenericStack<Integer> intStack = new GenericStack<>();
        intStack.push(1); // 1 автоупаковывается в new Integer(1)
        intStack.push(2);
        intStack.push(-2);
        print(intStack);
    }

    /**
     * Отображает объекты и освобождает стек
     */
    public static void print(GenericStack<?> stack) {
        while (!stack.isEmpty()) {
            System.out.print(stack.pop() + " ");
        }
    }
}