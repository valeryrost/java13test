package myProbe.generics;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        list1.add(14);
        list1.add(24);
        list1.add(14);
        list1.add(42);
        list1.add(25);
        ArrayList<Integer> newList = removeDuplicates(list1);
        System.out.print(newList);
        System.out.println();
        Integer[] list2 = {3, 4, 5, 1, -3, -5, -1};
        System.out.println(linearSearch(list2, 2));
        System.out.println(linearSearch(list2, 5));
        System.out.println();
        Integer[][] numbers = { {1, 2, 3}, {4, 4, 6} };
        System.out.println(max(numbers));
        ArrayList<Integer> list3 = new ArrayList<Integer>();
        list3.add(14);
        list3.add(24);
        list3.add(4);
        list3.add(42);
        list3.add(5);
        System.out.print(max(list3));
    }

    public static <E> ArrayList<E> removeDuplicates(ArrayList<E> list) {
        ArrayList<E> result = new ArrayList<E>();
        for (E e : list) {
            if (!result.contains(e))
                result.add(e);
        }
        return result;
    }

    public static <E extends Comparable<E>> int linearSearch(E[] list, E key) {
        for (int i = 0; i < list.length; i++)
            if (list[i].equals(key))
                return i;
        return -1;
    }

    public static<E extends Comparable<E>> E max(E[][] list) {
        E max = list[0][0];
        for (int i = 1; i < list.length; i++) {
            for (int j = 1; j < list[i].length; j++) {
                if (max.compareTo(list[i][j]) < 0) {
                    max = list[i][j];
                }
            }
        }
        return max;
    }

    public static <E extends Comparable<E>> E max(ArrayList<E> list) {
        E currentMax = list.get(0);
        for (int i = 1; i < list.size(); i++)
            if (currentMax.compareTo(list.get(i)) < 0)
                currentMax = list.get(i);
        return currentMax;
    }
}
