package myProbe.generics;

public class GenericSort {
    public static void main(String[] args) {
        // Создать массив типа Integer
        Integer[] intArray = {2, 4, Integer.valueOf(3)};
        // Создать массив типа Double
        Double[] doubleArray = {new Double(3.4), new Double(1.3), new Double(-22.1)};
        // Создать массив типа Character
        Character[] charArray = {Character.valueOf('а'), new Character('К'), new Character('р')};
        // Создать массив типа String
        String[] stringArray = {"Вера", "Надежда", "Любовь"};
        // Отсортировать массивы
        sort(intArray);
        sort(doubleArray);
        sort(charArray);
        sort(stringArray);
        // Отобразить отсортированные массивы
        System.out.print("Отсортированные объекты типа Integer: ");
        printList(intArray);
        System.out.print("Отсортированные объекты типа Double: ");
        printList(doubleArray);
        System.out.print("Отсортированные объекты типа Character: ");
        printList(charArray);
        System.out.print("Отсортированные объекты типа String: ");
        printList(stringArray);
    }

    /**
     * Сортирует массив сравнимых объектов
     */
    public static <E extends Comparable<E>> void sort(E[] list) {
        E currentMin;
        int currentMinIndex;
        for (int i = 0; i < list.length - 1; i++) {
            // Найти наименьший объект в list[i..list.length-1]
            currentMin = list[i];
            currentMinIndex = i;
            for (int j = i + 1; j < list.length; j++) {
                if (currentMin.compareTo(list[j]) > 0) {
                    currentMin = list[j];
                    currentMinIndex = j;
                }
            }
            // Переставляет list[i] с list[currentMinIndex] в случае необходимости
            if (currentMinIndex != i) {
                list[currentMinIndex] = list[i];
                list[i] = currentMin;
            }
        }
    }

    /**
     * Отображает массив объектов
     */
    public static void printList(Object[] list) {
        for (int i = 0; i < list.length; i++)
            System.out.print(list[i] + " ");
        System.out.println();
    }
}