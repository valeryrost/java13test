package myProbe.generics;

public class WildCardNeedDemo {
    public static void main(String[] args) {
        GenericStack<Integer> intStack = new GenericStack<>();
        intStack.push(1);     // 1 автоупаковывается в new Integer(1)
        intStack.push(2);
        intStack.push(-2);
        System.out.print("Наибольшее число равно " + max(intStack)); // Error:
    }

    /**
     * Найти в стеке наибольшее число
     */
    public static double max(GenericStack<? extends Number> stack) {
        double max = stack.pop().doubleValue(); // инициализирует max
        while (!stack.isEmpty()) {
            double value = stack.pop().doubleValue();
            if (value > max)
                max = value;
        }
        return max;
    }
}