package myProbe.classes2;

public class Test01 {
    public static void main(String[] args) {
        Integer x2 = new Integer(3);
        System.out.println(x2.intValue());
        System.out.println(x2.compareTo(new Integer(4)));

        System.out.println(Integer.parseInt("10"));
        System.out.println(Integer.parseInt("10", 10));
        System.out.println(Integer.parseInt("10", 16));
        System.out.println(Integer.parseInt("11"));
        System.out.println(Integer.parseInt("11", 10));
        System.out.println(Integer.parseInt("11", 16));

        Integer[] intArray = {1, 2, 3};
        System.out.println(intArray[0] + intArray[1] + intArray[2]);

        Double x1 = 3.5;
        System.out.println(x1.intValue());
        System.out.println(x1.compareTo(4.5));

        java.math.BigInteger x = new java.math.BigInteger("3");
        java.math.BigInteger y = new java.math.BigInteger("7");
        java.math.BigInteger z = x.add(y);
        System.out.println("x равно " + x);
        System.out.println("y равно " + y);
        System.out.println("z равно " + z);
    }
}
