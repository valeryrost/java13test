package myProbe.classes2;

public class Test04 {
    public static void main(String[] args) {
        String s = "Java";
        StringBuilder buffer = new StringBuilder(s);
        change(buffer);
        System.out.println(buffer);
    }

    private static void change(StringBuilder buffer) {
        buffer.append(" и HTML");
    }

    public class A extends B {
        A(String s) {super(s); };
    }

    class B {
        public B(String s) {
        }
    }
}