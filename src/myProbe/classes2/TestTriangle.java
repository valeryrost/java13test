package myProbe.classes2;


import java.util.Scanner;

public class TestTriangle {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите три стороны: ");
        double side1 = input.nextDouble();
        double side2 = input.nextDouble();
        double side3 = input.nextDouble();

        Triangle triangle = new Triangle(side1, side2, side3);

        System.out.print("Введите цвет: ");
        String color = input.next();
        triangle.setColor(color);

        System.out.print("Закрашена ли фигура (true/false)? ");
        boolean filled = input.nextBoolean();
        triangle.setFilled(filled);

        System.out.println("Площадь равна " + triangle.getArea());
        System.out.println("Периметр равен " + triangle.getPerimeter());
        System.out.println(triangle);
    }
}

