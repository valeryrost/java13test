package myProbe.classes1;

public class TestCircle {
    public static void main(String[] args) {
        System.out.println("Количество объектов: " + Circle.getNumberOfObjects());
        Circle circle1 = new Circle(); // Создать круг 1 с радиусом 1
        System.out.println("Площадь круга с радиусом " + circle1.getRadius() + " равна " + circle1.getArea());
        // Создать круг 2 с радиусом 25
        Circle circle2 = new Circle(25);
        System.out.println("Площадь круга с радиусом " + circle2.getRadius() + " равна " + circle2.getArea());
        // Создать круг 3 с радиусом 125
        Circle circle3 = new Circle(125);
        System.out.println("Площадь круга с радиусом " + circle3.getRadius() + " равна " + circle3.getArea());
        // Изменить радиус круга
        circle2.setRadius(100);
        System.out.println("Площадь круга с радиусом " + circle2.getRadius() + " равна " + circle2.getArea());
        System.out.println("Количество объектов: " + Circle.getNumberOfObjects());



    }
}
