package myProbe.classes1;

public class Rectangle {
    double width;
    double height;

    Rectangle() {
        width = -1;
        height = -1;
    }

    Rectangle(double newWidth, double newHeight) {
        width = newWidth;
        height = newHeight;
    }

    double getArea() {
        //return width * height;
        return Math.round(width * height * 100) / 100.0;
    }

    double getPerimeter() {
        return 2 * width + 2 * height;
    }


}
