package myProbe.classes1;

public class Location {
    public int row;
    public int column;
    public double maxValue;

    public static Location locateLargest(double[][] a) {
        Location location = new Location();
        location.row = 0;
        location.column = 0;
        location.maxValue = a[0][0];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                if (a[i][j] > location.maxValue) {
                    location.maxValue = a[i][j];
                    location.column = i;
                    location.row = j;
                }
            }
        }
        return location;
    }
}
