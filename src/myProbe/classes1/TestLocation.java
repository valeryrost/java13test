package myProbe.classes1;

import java.util.Scanner;

public class TestLocation {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите количество строчек и столбцов массива: ");
        int column = input.nextInt();
        int row = input.nextInt();
        double[][] a = new double[column][row];
        System.out.println("Введите массив:");
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                a[i][j] = input.nextDouble();
            }
        }
        Location location = Location.locateLargest(a);
        System.out.println("Наибольший элемент массива, равный " + location.maxValue +
                " находится в позиции (" + location.column + ", " + location.row + ")");
    }
}
