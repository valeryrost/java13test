package myProbe.classes1;

public class Stock {
    String symbol;  //для обозначения акций
    String name;   //для наименования акций
    double previousClosingPrice;  //стоимость акций на момент закрытия предыдущего дня
    double currentPrice;          //стоимость акций в настоящий момент

    Stock(String newSymbol, String newName, double newPreviousClosingPrice, double newCurrentPrice) {
        symbol = newSymbol;
        name = newName;
        previousClosingPrice = newPreviousClosingPrice;
        currentPrice = newCurrentPrice;
    }

    double getChangePercent() {
        return Math.round((currentPrice - previousClosingPrice) / previousClosingPrice * 100 * 100) / 100.0;
    }
}
