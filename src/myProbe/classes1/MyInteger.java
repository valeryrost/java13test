package myProbe.classes1;

public class MyInteger {
    private int value;

    public int getValue() {
        return value;
    }

    public MyInteger(int value) {
        this.value = value;
    }

    public boolean isPrime() {
        return isPrime(value);
    }

    public static boolean isPrime(int num) {
        if ((num == 1) || (num == 2)) {
            return true;
        }

        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                return false;
            }
        }

        return true;
    }

    public static boolean isPrime(MyInteger o) {
        return isPrime(o.getValue());
    }

    public boolean isEven() {
        return isEven(value);
    }

    public boolean isOdd() {
        return isOdd(value);
    }

    public static boolean isEven(int n) {
        return n % 2 == 0;
    }

    public static boolean isOdd(int n) {
        return n % 2 != 0;
    }

    public static boolean isEven(MyInteger n) {
        return isEven(n.getValue());
    }

    public boolean equals(int anotherNum) {
        return value == anotherNum;
    }

    public boolean equals(MyInteger o) {
        return value == o.getValue();
    }

    public static int parseInt(char[] numbers) {
        // numbers состоит из символов цифр.
        // Например, если numbers равно {'1', '2', '5'}, то возвращаемое значение
        // должно быть равным 125. Обратите внимание, что
        // numbers[0] равно '1'
        // numbers[1] равно '2'
        // numbers[2] равно '5'

        int result = 0;
        for (int i = 0; i < numbers.length; i++) {
            result = result * 10 + (numbers[i] - '0');
        }

        return result;
    }

    public static int parseInt(String s) {
        // s состоит из символов цифр.
        // Например, если s равно "125", то возвращаемое значение
        // должно быть равным 125.
        int result = 0;
        for (int i = 0; i < s.length(); i++) {
            result = result * 10 + (s.charAt(i) - '0');
        }

        return result;
    }
}
