package myProbe.classes1;

public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(4, 40);
        Rectangle rectangle2 = new Rectangle(3.5, 35.9);
        printRectangleData(rectangle1);
        printRectangleData(rectangle2);

    }

    public static void printRectangleData(Rectangle rectangle) {
        System.out.println("Щирина- " + rectangle.width + " Высота- " + rectangle.height +
                " Площадь- " + rectangle.getArea() + " Периметр- " + rectangle.getPerimeter());
    }
}
