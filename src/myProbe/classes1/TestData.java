package myProbe.classes1;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class TestData {
    public static void main(String[] args) {
        final int MILLISECONDS_PER_SECOND = 1000, SECONDS_PER_MINUTE = 60,
                MINUTES_PER_HOUR = 60, HOURS_PER_DAY = 24, MSK_TIME = 3;
        long totalMilliseconds, totalSeconds, currentSecond, totalMinutes, currentMinute, totalHours, currentHour;
        // Получить общее кол-во миллисекунд, прошедших с 00:00:00 GMT 01/01/1970
        totalMilliseconds = System.currentTimeMillis();
        // Вычислить общее кол-во секунд, прошедших с 00:00:00 GMT 01/01/1970
        totalSeconds = totalMilliseconds / MILLISECONDS_PER_SECOND;
        // Вычислить текущее кол-во секунд
        currentSecond = totalSeconds % SECONDS_PER_MINUTE;
        // Вычислить общее кол-во минут, прошедших с 00:00:00 GMT 01/01/1970
        totalMinutes = totalSeconds / SECONDS_PER_MINUTE;
        // Вычислить текущее кол-во минут
        currentMinute = totalMinutes % MINUTES_PER_HOUR;
        // Вычислить общее кол-во часов, прошедших с 00:00:00 GMT 01/01/1970
        totalHours = totalMinutes / MINUTES_PER_HOUR;
        // Вычислить текущее кол-во часов по Гринвичу
        currentHour = totalHours % HOURS_PER_DAY + MSK_TIME;
        // Отобразить текущее время по Гринвичу
        //-------------------------------------------------
        System.out.println("Текущее время равно " + currentHour + ":" + currentMinute + ":" + currentSecond + " GMT.");
        //-------------------------------------------------
        Date date = new Date();
        System.out.println(date.toString());
        System.out.println("Время, прошедшее с 1 января 1970 г., равно " + date.getTime() + " миллисекунд.");
        //-------------------------------------------------
        Random gen1 = new Random(3);
        Random gen2 = new Random(3);
        System.out.println("Из generator1: ");
        for (int i = 0; i < 10; i++)
            System.out.print(gen1.nextInt(1000) + " ");
        System.out.println();
        System.out.println("Из generator2: ");
        for (int i = 0; i < 10; i++)
            System.out.print(gen2.nextInt(1000) + " ");
        System.out.println();
        //-------------------------------------------------
        Random gen3 = new Random(1000);
        for (int i = 0; i < 50; i++) {
            System.out.print(gen3.nextInt(100) + " ");
        }
        System.out.println();
        //-------------------------------------------------
        Date date1 = new Date();
        printSetDate(date1, 10000);
        printSetDate(date1, 100000);
        printSetDate(date1, 1000000);
        printSetDate(date1, 10000000);
        printSetDate(date1, 100000000);
        printSetDate(date1, 1000000000);
        printSetDate(date1, 10000000000L);
        printSetDate(date1, 100000000000L);
        //-------------------------------------------------
        GregorianCalendar greCalendar = new GregorianCalendar();
        System.out.println(greCalendar.get(GregorianCalendar.YEAR) + " -year " +
                (greCalendar.get(GregorianCalendar.MONTH) + 1) + " -month " +
                greCalendar.get(GregorianCalendar.DAY_OF_MONTH) + " - day ");
        greCalendar.setTimeInMillis(1234567898765L);
        System.out.println(greCalendar.get(GregorianCalendar.YEAR) + " -year " +
                (greCalendar.get(GregorianCalendar.MONTH) + 1) + " -month " +
                greCalendar.get(GregorianCalendar.DAY_OF_MONTH) + " - day ");

    }

    public static void printSetDate(@NotNull Date dateToSet, long timeToSet) {
        dateToSet.setTime(timeToSet);
        System.out.println(dateToSet.toString() + " " + timeToSet);
    }
}
