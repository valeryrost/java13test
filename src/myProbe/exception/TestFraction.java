package myProbe.exception;

public class TestFraction {
    public static void main(String[] args) {
        try {
            Fraction fraction = new Fraction(1, 0);
            System.out.println(fraction);
        } catch (NullDenominatorException e) {
            System.out.println(e.getMessage());
        }
    }
}
