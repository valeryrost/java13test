package myProbe.exception;

public class NullDenominatorException extends Exception {
    /** Создает исключение с сообщением message */
    public NullDenominatorException(String message) {
        super(message);
    }
}
