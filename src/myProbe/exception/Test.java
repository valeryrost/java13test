package myProbe.exception;

public class Test {
    public static void main(String[] args) {

        try {
            int zero = 0;
            int y = 2 / zero;
            try {

                String s = "5.6";
                Integer.parseInt(s);
            } catch (Exception e) {
            }
        } catch (RuntimeException e) {
            System.out.println(e);
        }
    }
}