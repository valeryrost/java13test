package myProbe.part2;

import java.util.Arrays;

public class Probe6 {
    public static void main(String[] args) {
        xMethod(5);
        System.out.println();
        xxMethod(5);
        System.out.println();
        System.out.println("топот - это палиндром? " + isPalindrome("топот"));
        double[] list = {2, 1, 3, 1, 2, 5, 2, -1, 0};
        sort(list);
        System.out.println(Arrays.toString(list));
        for (int i = 0; i < list.length; i++)
            System.out.print(list[i] + " ");
        int[] list1 = {3, 5, 7, 8, 12, 17, 24, 29};
        System.out.println();
        System.out.println(recursiveBinarySearch(list1, 7));
        System.out.println(recursiveBinarySearch(list1, 0));

    }

    public static void xMethod(int n) {
        if (n > 0) {
            System.out.print(n + " ");
            xMethod(n - 1);
        }
    }

    public static void xxMethod(int n) {
        if (n > 0) {
            xxMethod(n - 1);
            System.out.print(n + " ");
        }
    }

    public static boolean isPalindrome(String s) {
        if (s.length() <= 1) // простой случай
            return true;
        else if (s.charAt(0) != s.charAt(s.length() - 1)) // простой случай
            return false;
        else
            return isPalindrome(s.substring(1, s.length() - 1));
    }

    public static void sort(double[] list) {
        sort(list, 0, list.length - 1); // сортирует весь массив
    }

    private static void sort(double[] list, int low, int high) {
        if (low < high) {
            // Найти наименьший элемент и его индекс в list(low .. high)
            int indexOfMin = low;
            double min = list[low];
            for (int i = low + 1; i <= high; i++) {
                if (list[i] < min) {
                    min = list[i];
                    indexOfMin = i;
                }
            }
            // Переставить наименьшее число в list(low .. high) и list(low)
            list[indexOfMin] = list[low];
            list[low] = min;
            sort(list, low + 1, high);   // Отсортировать оставшийся list(low+1 .. high)
        }
    }

    public static int recursiveBinarySearch(int[] list, int key) {
        int low = 0;
        int high = list.length - 1;
        return recursiveBinarySearch(list, key, low, high);
    }

    private static int recursiveBinarySearch(int[] list, int key, int low, int high) {
        if (low > high) // поиск завершается без единого совпадения
            return -low - 1;
        int mid = (low + high) / 2;
        if (key < list[mid])
            return recursiveBinarySearch(list, key, low, mid - 1);
        else if (key == list[mid])
            return mid;
        else
            return recursiveBinarySearch(list, key, mid + 1, high);
    }

}

