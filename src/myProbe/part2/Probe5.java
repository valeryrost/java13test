package myProbe.part2;

import java.util.Scanner;

public class Probe5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        // Получить индекс числа Фибоначчи
        System.out.print("Введите индекс: ");
        int index = input.nextInt();
        // Найти и отобразить число Фибоначчи
        System.out.println("Число Фибоначчи с индексом " + index + " равно " + fib(index));
        System.out.println("Сумма " + index + " чисел равна " + xSum(index));

    }

    /**
     * Находит число Фибоначчи
     */
    public static long fib(long index) {
        if (index == 0) // простой случай
            return 0;
        else if (index == 1) // простой случай
            return 1;
        else  // упрощение и рекурсивные вызовы
            return fib(index - 1) + fib(index - 2);
    }

    /**
     * Сумма n чисел
     *
     * @param n
     * @return
     */
    public static int xSum(int n) {
        if (n == 1)
            return 1;
        else
            return n + xSum(n - 1);
    }

}

