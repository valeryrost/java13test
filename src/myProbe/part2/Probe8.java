package myProbe.part2;

public class Probe8 {
    public static void main(String[] args) {
        System.out.println("ff");

        try {
            int value = 50;
            if (value < 40)
                throw new Exception("слишком маленькое значение");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println("Продолжить после блока catch");


        for (int i = 0; i < 2; i++) {
            System.out.print(i + " ");
            try {
                System.out.println(1 / 0);
            } catch (Exception ex) {
            }


        }

        try {
            for (int i = 0; i < 2; i++) {
                System.out.print(i + " ");
                System.out.println(1 / 0);
            }
        }
        catch (Exception ex) {
        }




    }
}