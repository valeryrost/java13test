package myProbe.part2;

import java.util.Scanner;

public class Probe2 {

    static final double ROUBLES_PER_DOLLAR = 72.12; // курс покупки

    public static void main(String[] args) {
        int[] dollarsArray;     // сумма денег в американских долларах
        double[] roublesArray;  // сумма денег в российских рублях
        int n;
        int i;
        Scanner input = new Scanner(System.in);
        istruct();
        do {
            System.out.print("Введите количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);
        System.out.print("Введите " + n + " сумм в долларах через пробел: ");
        dollarsArray = new int[n];
        for (i = 0; i < n; ++i)
            dollarsArray[i] = input.nextInt();
        roublesArray = find_roubles(dollarsArray, n);
        System.out.println("\n Сумма,р.\tСумма,$");
        for (i = 0; i < n; ++i)
            System.out.println("\t" + dollarsArray[i] + "\t" + (int) (roublesArray[i] * 100) / 100.0);
    }

    /**
     * Отображает инструкцию
     */
    public static void istruct() {
        System.out.println("Программа конвертирует доллары в рубли");
        System.out.println("Курс покупки: " + ROUBLES_PER_DOLLAR + " рубля. \n");
    }

    /**
     * Конвертирует доллары в рубли
     */
    public static double[] find_roubles(int[] dollarsArray, int n) {
        double[] roublesArray = new double[n];
        int i;
        for (i = 0; i < n; ++i)
            roublesArray[i] = ROUBLES_PER_DOLLAR * dollarsArray[i];
        return roublesArray;
    }
}

