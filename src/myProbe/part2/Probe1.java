package myProbe.part2;

import java.util.Arrays;

public class Probe1 {
    public static void main(String[] args) {
        int[] list1 = {4, 5, 1, 2, 9, -3};
        System.out.println(linearSearch(list1, 2));

        int[] list2 = {-3, 1, 2, 4, 9, 23};
        System.out.println(binarySearch(list2, 3));

        int[] list3 = {2, 4, 7, 10, 11, 45, 50, 59, 60, 66, 69, 70, 79};
        System.out.println(binarySearch(list3, 11));

        double[] list4 = {-2, 4.5, 5, 1, 2, -3.3};
        printArray(list4);
        selectionSort(list4);
        printArray(list4);
        selectionSortDec(list4);
        printArray(list4);
        Arrays.sort(list4);
        printArray(list4);
        System.out.println(Arrays.toString(list4));
        binarySearch(new int[]{1, 4, 6, 8, 10, 15, 20}, 11);

    }

    /**
     * Применяет метод линейного поиска ключа в массиве
     */
    public static int linearSearch(int[] list, int key) {
        for (int i = 0; i < list.length; i++) {
            if (key == list[i])
                return i;
        }
        return -1;
    }

    /**
     * Применяет метод бинарного поиска ключа в массиве
     */
    public static int binarySearch(int[] list, int key) {
        int low = 0;
        int high = list.length - 1;
        while (high >= low) {
            int mid = low + (high - low) / 2;
            if (key < list[mid])
                high = mid - 1;
            else if (key == list[mid])
                return mid;
            else
                low = mid + 1;
        }
        return -low - 1; // Теперь high < low
    }

    /**
     * Сортирует массив методом выбора
     */
    public static void selectionSort(double[] list) {
        for (int i = 0; i < list.length - 1; i++) {
            // Найти наименьшее значение в list[i..list.length-1]
            double currentMin = list[i];
            int currentMinIndex = i;
            for (int j = i + 1; j < list.length; j++) {
                if (currentMin > list[j]) {
                    currentMin = list[j];
                    currentMinIndex = j;
                }
            }
            // Переставить list[i] и list[currentMinIndex], если необходимо
            if (currentMinIndex != i) {
                list[currentMinIndex] = list[i];
                list[i] = currentMin;
            }
        }
    }

    /**
     * Сортирует массив методом выбора (убывание)
     */
    public static void selectionSortDec(double[] list) {
        for (int i = 0; i < list.length - 1; i++) {
            double currentMax = list[i];
            int currentMaxIndex = i;
            for (int j = i + 1; j < list.length; j++) {
                if (currentMax < list[j]) {
                    currentMax = list[j];
                    currentMaxIndex = j;
                }
            }
            if (currentMaxIndex != i) {
                list[currentMaxIndex] = list[i];
                list[i] = currentMax;
            }
        }
    }


    /**
     * Печать массива
     *
     * @param array
     */
    public static void printArray(double[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

}
