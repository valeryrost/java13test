package myProbe.temp;

import java.io.*;

public class Test {
    public static void main(String[] args) {
        String path = "G:\\BITBUCKET\\One\\ProjectOne\\src\\myProbe\\temp\\Test.txt";
        String path1 = "G:\\BITBUCKET\\One\\ProjectOne\\src\\myProbe\\temp\\Test1.txt";
        File file = new File(path);
        File file1 = new File(path1);
        System.out.println("File exists- " + file.exists());
        try {
            if (file.createNewFile())
                System.out.println("File created");
        } catch (IOException e) {
            System.out.println("Error " + e.getMessage());
        }
        System.out.println("File exists- " + file.exists());
        System.out.println("isDirectory- " + file.isDirectory());
        System.out.println("isFile- " + file.isFile());
        System.out.println("getName- " + file.getName());
        System.out.println("getPath- " + file.getPath());
        FileWriter writer;
        try {
            writer = new FileWriter(file,true);
            writer.write("Hello world \n");
            writer.close();
        } catch (IOException e) {
            System.out.println("Error " + e.getMessage());
        }
        FileReader reader;
        try {
            reader = new FileReader(file);
            int symbol  = reader.read();
            while (symbol!=-1){
                System.out.print((char) symbol);
                symbol = reader.read();
            }
            reader.close();
        } catch (IOException exception) {
            System.out.println("Error " + exception.getMessage());
        }

    }
}
