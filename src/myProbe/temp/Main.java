package myProbe.temp;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        strings.add("ff");
        strings.add("dddd");
        strings.add("eeee");
        Predicate<String> predicate = s -> s.length() > 2;

        for (String s : strings) {
            if (predicate.test(s))
                System.out.println(s);
        }
    }
}
