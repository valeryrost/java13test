package myProbe.part1;

import java.util.Scanner;

public class Probe12 {
    public static void main(String[] args) {

        final int SENTINEL = -99;
        Scanner input = new Scanner(System.in);
        int score;
        int sum = 0;

        System.out.print("Введите баллы за первый экзамен или " + SENTINEL + " для выхода: ");

        for (score = input.nextInt(); score != SENTINEL; score = input.nextInt()) {
            sum += score;
            System.out.print("Введите баллы за следующий экзамен или " + SENTINEL + " для выхода: ");
        }

        System.out.println("Сумма баллов за все экзамены равна " + sum);
    }
}
