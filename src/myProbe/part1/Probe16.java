package myProbe.part1;

import java.util.Scanner;

public class Probe16 {
    public static void main(String[] args) {

        String s1, s2 = "";
        Scanner input = new Scanner(System.in);
        System.out.print("Введите строку: ");
        s1 = input.nextLine();
        int index = s1.length() - 1;
        while (index >= 0) {
            s2 = s2 + s1.charAt(index);
            index--;
        }
        System.out.println("исходная " + s1);
        System.out.println("итоговая " + s2);

    }
}
