package myProbe.part1;
//        Напишите программу, которая конвертирует сумму денег
//        из американских долларов в российские рубли по курсу покупки 72.12.

import java.util.Scanner;

public class Probe5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        final double ROUBLES_PER_DOLLAR = 72.12; // курс покупки
        int dollars; // сумма денег в американских долларах
        double roubles; // сумма денег в российских рублях
        int digit; // последняя цифра dollars

        System.out.println("Введите сумму в долларах: ");
        dollars = input.nextInt();

        System.out.print(dollars);

        if (5 <= dollars && dollars <= 20) {
            System.out.print(" долларов равны ");
        } else {
            digit = dollars % 10;
            if (digit == 1)
                System.out.print(" доллар равен ");
            else if (2 <= digit && digit <= 4)
                System.out.print(" доллара равны ");
            else
                System.out.print(" долларов равны ");

        }

        roubles = ROUBLES_PER_DOLLAR * dollars;

        System.out.println((int) (roubles * 100) / 100.0+" рубля");

        boolean even = false;
        System.out.println((even ? "истина" : "ложь"));
        int x = 1;
        int y = x = x + 1;
        System.out.println("y равен " + y);

    }
}
