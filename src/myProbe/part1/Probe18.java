package myProbe.part1;

import java.util.Scanner;

public class Probe18 {
    static final double ROUBLES_PER_DOLLAR = 72.12; // курс покупки

    public static void main(String[] args) {
        int dollars;     // сумма денег в американских долларах
        double roubles;  // сумма денег в российских рублях
        int digit;       // последняя цифра dollars
        int n;
        int i;
        Scanner input = new Scanner(System.in);
        istruct();
        do {
            System.out.print("Введите количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);
        for (i = 0; i < n; ++i) {
            System.out.print("Введите сумму в долларах: ");
            dollars = input.nextInt();
            System.out.print(dollars);
            if (5 <= dollars && dollars <= 20) {
                System.out.print(" долларов равны ");
            } else {
                digit = dollars % 10;
                if (digit == 1)
                    System.out.print(" доллар равен ");
                else if (2 <= digit && digit <= 4)
                    System.out.print(" доллара равны ");
                else
                    System.out.print(" долларов равны ");
            }
            roubles = find_roubles(dollars);
            System.out.println((int) (roubles * 100) / 100.0 + " рубля");
        }
    }

    /**
     * Отображает инструкцию
     */
    public static void istruct() {
        System.out.println("Программа конвертирует доллары в рубли");
        System.out.println("Курс покупки: " + ROUBLES_PER_DOLLAR + " рубля. \n");
    }

    /**
     * Конвертирует доллары в рубли
     */
    public static double find_roubles(int dollars) {
        return ROUBLES_PER_DOLLAR * dollars;
    }
}
