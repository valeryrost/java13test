package myProbe.part1;

import java.util.Scanner;

public class Probe11 {
    public static void main(String[] args) {

        char x = 'a';
        char y = 'c';
        System.out.println(++x);
        System.out.println(y++);
        System.out.println("SELECT".substring(4, 4));
        /*--------------------------------------------------------*/
        int n, sum = 0, i = 0;
        Scanner input = new Scanner(System.in);
        System.out.print("Введите n для вычисления суммы 1 + 2 + 3 + ... + n: ");
        n = input.nextInt();
        while (i < n) {
            i++;
            sum += i;
        }

        System.out.println("Сумма целых чисел от 1 до " + n + " равна " + sum);
        if (sum == (n * (n + 1)) / 2)
            System.out.print("Это эквивалентно ");  // сообщение №1
        else
            System.out.print("Это не эквивалентно ");  // сообщение №2
        System.out.print("(n * (n + 1)) / 2 для n, равного " + n);


    }
}
