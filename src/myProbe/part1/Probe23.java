package myProbe.part1;


public class Probe23 {
    public static void main(String[] args) {
        int[] testArray = new int[5];
        int[] sourceArray = {2, 3, 1, 5, 10};
        System.out.println(testArray.length);
        System.out.println(sourceArray.length);

        System.arraycopy(sourceArray, 0, testArray, 0, sourceArray.length);
        printArray(sourceArray);
        printArray(testArray);
        printArray(new int[]{3, 1, 2, 6, 4, 2, 2, 2});
        testArray = reverse(testArray);
        printArray(testArray);
        for (int i = 0, j = testArray.length - 1; i < testArray.length / 2; i++, j--) {
            int temp = testArray[i];
            testArray[i] = testArray[j];
            testArray[j] = temp;
        }
        printArray(testArray);


    }

    /**
     * Печать массива
     *
     * @param array
     */
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    /**
     * Переставляет массив
     *
     * @param list
     * @return
     */
    public static int[] reverse(int[] list) {
        int[] result = new int[list.length];
        for (int i = 0, j = result.length - 1; i < list.length; i++, j--)
            result[j] = list[i];
        return result;
    }
}
