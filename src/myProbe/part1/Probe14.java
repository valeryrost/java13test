package myProbe.part1;

public class Probe14 {
    public static void main(String[] args) {
//----------------------------------------------
        for (int i = 1; i < 5; i++) {
            int j = 0;
            while (j < i) {
                System.out.print(j + " ");
                j++;
            }
        }
//---------------------------------------------
        System.out.println();
        int i = 0;
        while (i < 5) {
            for (int j = i; j > 1; j--)
                System.out.print(j + " ");
            System.out.println("****");
            i++;
        }
//--------------------------------------------------
        System.out.println();
        // отобразить верхнюю половину и центральный ряд
        for (i = 1; i <= 6; ++i) {
            for (int j = 0; j < i; ++j)
                System.out.print("  " + j);
            System.out.println();
        }
        // отобразить нижнюю половину
        for (i = 5; i > 0; --i) {
            for (int j = 0; j < i; ++j)
                System.out.print("  " + j);
            System.out.println();
        }

    }
}
